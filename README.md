# Multitemporal and multispectral data fusion for super-resolution of Sentinel-2 images

**Tomasz Tarasiewicz, Student Member, IEEE, Jakub Nalepa, Member, IEEE, Reuben A. Farrugia, Senior Member, IEEE, Gianluca Valentino, Senior Member, IEEE, Mang Chen, Johann A. Briffa, and Michal Kawulok, Senior Member, IEEE**

This repository contains supplementary material to the manuscript submitted to IEEE Transactions on Geoscience and Remote Sensing:

- **predictions/** &mdash; a directory containing raw predictions for every scene presented in the manuscript,
- **results/** &mdash; a directory that contains Excel spreadsheets with results for all experiments carried out within the manuscript,

