class BlankOptimizer:
    def __init__(self, *args, **kwargs):
        pass

    def step(self, closure=None):
        pass

    def zero_grad(self, set_to_none: bool = False):
        pass

    def state_dict(self):
        return dict()

    def load_state_dict(self, state_dict):
        pass