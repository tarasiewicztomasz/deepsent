"""
Individual runners i.e. classes that execute epochs in a training process.

"""
import array
import math
import statistics
import typing
import abc
import torch

from torch.utils import data
from torch import optim, nn
from torch.cuda.amp import GradScaler, autocast
from superdeep.utilities import torch_utils, observers
from superdeep.training import events
from superdeep.data import MultiBandSREntry

Criterion = nn.Module


class EpochRunner(abc.ABC):
    """
    Base class defining an interface for running a single pass through the whole input dataset
    """

    @abc.abstractmethod
    def run_epoch(self, model: nn.Module, data_loader: data.DataLoader, epoch: int) -> float:
        """
        :param model: Model to run epoch on.
        :param data_loader: Loader return batches of tuples (input, label)
        :return: Epoch loss
        """

    def get_optimizer(self):
        raise NotImplementedError


class TrainingEpochRunner(EpochRunner, observers.Observable):
    """
    Runs training procedure, executing backward propagation.
    Observers are notified.
    """

    def __init__(self,
                 optimizer: optim.Optimizer,
                 criterion: Criterion,
                 device: str = torch_utils.get_default_processing_device(),
                 post_prediction_func: typing.Callable = None,
                 mixed_precision_training: bool = False,
                 **kwargs):
        observers.Observable.__init__(self)
        self._optimizer = optimizer
        self._criterion = criterion
        self._device = device
        self._kwargs = kwargs
        self._post_prediction_func = post_prediction_func
        self.mixed_precision = mixed_precision_training
        self.scaler = GradScaler(enabled=mixed_precision_training)

    def get_optimizer(self):
        return self._optimizer

    def run_epoch(self, model: nn.Module, data_loader: data.DataLoader, epoch: int):
        model.train(True)
        batch_losses = {'total': array.array('d', [])}
        self.notify(events.EpochEvents.EPOCH_STARTED, (epoch, len(data_loader.dataset), data_loader.batch_size))
        for batch_iterator, entry in enumerate(data_loader):
            entry.to(self._device)
            self._optimizer.zero_grad()
            with autocast(self.mixed_precision):
                output = model(entry, **self._kwargs)
                if self._post_prediction_func is not None:
                    output = self._post_prediction_func(lr=entry.lr_images, sr=output, hr=entry.hr_image,
                                                        device=self._device, **self._kwargs)
                if not isinstance(entry, MultiBandSREntry) and entry.band is not None:
                    output = {entry.band: output}
                    entry.hr_image = {entry.band: entry.hr_image}
                    entry.hr_mask = {entry.band: entry.hr_mask}
                raw_loss = self._criterion(output, entry.hr_image, entry.hr_mask)
                if isinstance(raw_loss, dict):
                    loss = torch.mean(torch.stack(list(raw_loss.values())))
                    for k, v in raw_loss.items():
                        if k not in batch_losses.keys():
                            batch_losses[k] = array.array('d', [])
                        batch_losses[k].append(v.item())
                else:
                    loss = raw_loss
                batch_losses['total'].append(loss.item())
            # if loss.grad is not None:
            self.scaler.scale(loss).backward()
            self.scaler.step(self._optimizer)
            self.scaler.update()
            self.notify(events.EpochEvents.BATCH_FINISHED,
                        [epoch, batch_iterator,
                         statistics.mean(batch_losses['total']) if batch_losses['total'] else 0.0])
        results = {k: statistics.mean(v) if v else 0.0 for k, v in batch_losses.items()}
        self.notify(events.EpochEvents.EPOCH_FINISHED, None)
        return results


class ValidationEpochRunner(EpochRunner, observers.Observable):
    """
    Passes through validation dataset without backward propagation.
    Observers are notified.
    """

    def __init__(self,
                 criterion: Criterion, device: str = torch_utils.get_default_processing_device(), **kwargs):
        observers.Observable.__init__(self)
        self._criterion = criterion
        self._device = device
        self._kwargs = kwargs

    def get_optimizer(self):
        return None

    def run_epoch(self, model: nn.Module, data_loader: data.DataLoader, epoch: int):
        model.train(False)
        batch_losses = {'total': array.array('d', [])}
        self.notify(events.EpochEvents.EPOCH_STARTED, (epoch, len(data_loader.dataset), data_loader.batch_size))
        with torch.no_grad():
            for batch_iterator, entry in enumerate(data_loader):
                entry.to(self._device)
                output = model(entry,  **self._kwargs)
                if not isinstance(entry, MultiBandSREntry) and entry.band is not None:
                    output = {entry.band: output}
                    entry.hr_image = {entry.band: entry.hr_image}
                    entry.hr_mask = {entry.band: entry.hr_mask}
                self.notify(events.EpochEvents.VALIDATION_BATCH_OUTPUT,
                            observers.EpochValidationResults(epoch, batch_iterator, output))
                raw_loss = self._criterion(output, entry.hr_image, hr_mask=entry.hr_mask)
                if isinstance(raw_loss, dict):
                    loss = torch.mean(torch.stack(list(raw_loss.values())))
                    for k, v in raw_loss.items():
                        if k not in batch_losses.keys():
                            batch_losses[k] = array.array('d', [])
                        batch_losses[k].append(v.item())
                else:
                    loss = raw_loss
                batch_losses['total'].append(loss.item())
                self.notify(events.EpochEvents.BATCH_FINISHED,
                            [epoch, batch_iterator,
                             statistics.mean(batch_losses['total']) if batch_losses['total'] else 0.0])
            results = {k: statistics.mean(v) if v else 0.0 for k, v in batch_losses.items()}
            self.notify(events.EpochEvents.EPOCH_FINISHED, None)
        return results
