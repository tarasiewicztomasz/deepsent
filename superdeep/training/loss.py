"""
Module defines additional losses which can be used during training of models,
when the most popular MSE is not enough.

"""
import torch
from torch import nn
from typing import Union, List
from itertools import product


class SRLoss(nn.Module):
    def __init__(self, reduce_bands: bool = False, name: str = None,**kwargs):
        super().__init__()
        self.name = self.__class__.__name__ if name is None else name
        self.reduce_bands = reduce_bands

    @property
    def is_spectral(self):
        return False

    @property
    def best_min(self) -> bool:
        raise NotImplementedError

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, new_name):
        self._name = new_name

    def forward(self, sr, hr, hr_mask=None, lr_images=None, **kwargs):
        assert type(sr) == type(hr), f"Super-resolved image ({type(sr)}) is not the same type" \
                                     f" as target HR image ({type(hr)})!"
        if isinstance(sr, dict):
            if hr_mask is None:
                hr_mask = {k: None for k in sr.keys()}
            results = {k: self._forward(sr[k], hr[k], hr_mask[k]) for k in sr.keys() if hr[k] is not None}
            if self.reduce_bands:
                results = torch.mean(torch.stack(list(results.values())))
            return results
        return self._forward(sr, hr, hr_mask)

    def _forward(self, sr, hr, hr_mask=None, lrs=None, **kwargs):
        raise NotImplementedError

    @staticmethod
    def mask_pixels(sr, hr, hr_mask=None):
        if hr_mask is None:
            hr_mask = torch.ones_like(hr)
        sr = sr * hr_mask
        hr = hr * hr_mask
        total_unmasked = torch.sum(hr_mask, dim=(-3, -2, -1))
        return sr, hr, total_unmasked


class Charbonnier(SRLoss):
    """
    Charbonnier loss as described in
    "Fast and Accurate Image Super-Resolution withDeep Laplacian Pyramid Networks".
    """
    EPSILON = 1e-6

    def __init__(self):
        super().__init__()

    def _forward(self, X, Y, *args, **kwargs):
        diff = torch.add(X, -Y)
        error = torch.sqrt(diff * diff + self.EPSILON)
        loss = torch.sum(error)
        return loss


class LossCombinerBase(SRLoss):
    @property
    def best_min(self):
        return None

    def __init__(self, losses: Union[SRLoss, List[SRLoss]], training_mode: bool = True):
        super().__init__()
        self.train(training_mode)
        if not isinstance(losses, list):
            losses = [losses]
        self.losses = losses
        self._rename_losses()

    def __iter__(self):
        return iter(self.losses)

    @property
    def name(self):
        if len(self.losses) == 1:
            return self.losses[0].name
        return super()._name

    @name.setter
    def name(self, new_name):
        self._name = new_name

    def _forward(self, sr, hr, hr_mask=None, lrs=None, **kwargs):
        raise NotImplementedError

    def _rename_losses(self):
        raise NotImplementedError


class cLossCombiner(LossCombinerBase):
    def _rename_losses(self):
        for loss in self.losses:
            loss.name = f"c{loss.name}"

    def __init__(self, losses: Union[SRLoss, List[SRLoss]], training_mode: bool = True):
        super().__init__(losses, training_mode)
        self.loss_choice_functions = {loss: torch.min if loss.best_min else torch.max for loss in self.losses}

    def _forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor = None, lrs=None, **kwargs):
        height, width = hr.shape[-2:]
        if hr_mask is None:
            hr_mask = torch.ones_like(hr)
        border = 3
        max_pixel_shifts = 2 * border
        cropped_height, cropped_width = height - max_pixel_shifts, width - max_pixel_shifts
        sr_patch = sr[:, :, border:height - border, border:width - border]
        hr_patches = []
        mask_patches = []
        for i, j in product(range(max_pixel_shifts + 1), range(max_pixel_shifts + 1)):
            hr_patches.append(hr[:, :, i:i + cropped_height,
                              j:j + cropped_width])
            mask_patches.append(hr_mask[:, :, i:i + cropped_height,
                                j:j + cropped_width])

        hr_patches = torch.stack(hr_patches, dim=1)
        mask_patches = torch.stack(mask_patches, dim=1)
        sr_patch = torch.repeat_interleave(sr_patch.unsqueeze(1), hr_patches.shape[1], dim=1)
        total_unmasked = torch.sum(mask_patches, dim=[2, 3, 4]).view(-1, hr_patches.shape[1], 1, 1, 1)
        b = torch.pow(total_unmasked, -1) * torch.sum(hr_patches - sr_patch, dim=[2, 3, 4]).view(total_unmasked.shape)
        sr_patch = sr_patch + b
        results = {}
        for loss, choice_function in self.loss_choice_functions.items():
            loss_values = loss(sr_patch, hr_patches, mask_patches)
            results[loss.name] = torch.mean(choice_function(loss_values, dim=1)[0])
        if len(list(results.keys())) == 1:
            results = list(results.values())[0]
        return results


class MGE(SRLoss):
    # TODO: implement
    @property
    def best_min(self) -> bool:
        return True

    def __init__(self):
        super().__init__()
        pass

    def _forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor = None, **kwargs):
        pass


class cMSE(SRLoss):
    @property
    def best_min(self) -> bool:
        return True

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor = None, **kwargs):
        if hr_mask is None:
            return self.unmasked_forward(sr, hr)
        return self.masked_forward(sr, hr, hr_mask)

    def unmasked_forward(self, sr: torch.Tensor, hr: torch.Tensor):
        hr_size = hr.shape[-2:]
        border = 3
        max_pixel_shifts = 2 * border
        size_cropped_image = (hr_size[0] - max_pixel_shifts, hr_size[1] - max_pixel_shifts)
        cropped_predictions = sr[:, :, border:hr_size[0] - border, border:hr_size[1] - border]
        x = []

        for i in range(max_pixel_shifts + 1):
            for j in range(max_pixel_shifts + 1):
                cropped_labels = hr[:, :, i:i + size_cropped_image[0],
                                 j:j + size_cropped_image[1]]
                total_pixels = cropped_labels.numel()
                b = (1.0 / total_pixels) * torch.sum(torch.sub(cropped_labels, cropped_predictions), dim=[1, 2, 3])
                b = b.view(-1, 1, 1, 1)
                corrected_cropped_predictions = cropped_predictions + b
                corrected_mse = (1.0 / total_pixels) * torch.sum(
                    torch.square(
                        torch.sub(cropped_labels, corrected_cropped_predictions)
                    ), dim=[1, 2, 3])
                x.append(corrected_mse)
        x = torch.stack(x)
        min_cmse = torch.min(x, 0)[0]
        return torch.mean(min_cmse)

    def masked_forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor):
        hr_size = hr.shape[-2:]
        border = 3
        max_pixel_shifts = 2 * border
        crop_size = (hr_size[0] - max_pixel_shifts, hr_size[1] - max_pixel_shifts)
        cropped_predictions = (sr[:, :, border:hr_size[0] - border, border:hr_size[1] - border]).type(torch.float32)
        x = []

        for i in range(max_pixel_shifts + 1):
            for j in range(max_pixel_shifts + 1):
                cropped_labels = hr[:, :, i:i + crop_size[0],
                                 j:j + crop_size[1]]
                cropped_hr_mask = hr_mask[:, :, i:i + crop_size[0],
                                  j:j + crop_size[1]]
                masked_cropped_predictions = cropped_predictions * cropped_hr_mask
                masked_cropped_labels = cropped_labels * cropped_hr_mask
                total_pixels_masked = torch.sum(cropped_hr_mask, dim=[1, 2, 3])
                b = (1.0 / total_pixels_masked) * torch.sum(torch.sub(masked_cropped_labels,
                                                                      masked_cropped_predictions), dim=[1, 2, 3])
                b = b.view(-1, 1, 1, 1)
                corrected_cropped_predictions = (masked_cropped_predictions + b) * cropped_hr_mask
                corrected_mse = (1.0 / total_pixels_masked) * torch.sum(
                    torch.square(
                        torch.sub(masked_cropped_labels, corrected_cropped_predictions)
                    ), dim=[1, 2, 3])
                x.append(corrected_mse)
        x = torch.stack(x)
        min_cmse = torch.min(x, 0)[0]
        return torch.mean(min_cmse)


class MSE(SRLoss):
    @property
    def best_min(self) -> bool:
        return True

    def _forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor = None, **kwargs):
        """
        Masked version of MSE (L2) loss.
        :param **kwargs:
        """
        sr, hr, total_unmasked = self.mask_pixels(sr, hr, hr_mask)
        result = torch.sum(torch.square(hr - sr), dim=(-3, -2, -1)) / total_unmasked
        return result


class L1(SRLoss):
    @property
    def best_min(self) -> bool:
        return True

    def _forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor = None, **kwargs):
        """
        Masked version of MAE (L1) loss.
        """
        sr, hr, total_unmasked = self.mask_pixels(sr, hr, hr_mask)
        l1_loss = torch.sum(torch.abs(hr - sr), dim=(-3, -2, -1)) / total_unmasked
        return l1_loss


class cL1(SRLoss):
    def _forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor = None, **kwargs):
        if hr_mask is None:
            hr_mask = sr.new_ones(sr.shape)
        hr_size = hr.shape[-1]
        border = 3
        max_pixel_shifts = 2 * border
        size_cropped_image = hr_size - max_pixel_shifts
        cropped_predictions = sr[:, :, border:hr_size - border, border:hr_size - border]
        x = []

        for i in range(max_pixel_shifts + 1):
            for j in range(max_pixel_shifts + 1):
                cropped_labels = hr[:, :, i:i + size_cropped_image,
                                 j:j + size_cropped_image]
                cropped_y_mask = hr_mask[:, :, i:i + size_cropped_image,
                                 j:j + size_cropped_image].type(torch.float32)
                cropped_predictions_masked = (cropped_predictions * cropped_y_mask).type(torch.float32)
                cropped_labels_masked = (cropped_labels * cropped_y_mask).type(torch.float32)
                total_pixels_masked = torch.sum(cropped_y_mask, dim=[1, 2, 3])
                b = (1.0 / total_pixels_masked) * torch.sum(torch.sub(cropped_labels_masked,
                                                                      cropped_predictions_masked), dim=[1, 2, 3])
                b = b.unsqueeze(-1).unsqueeze(-1).unsqueeze(-1)
                corrected_cropped_predictions = (cropped_predictions_masked + b) * cropped_y_mask
                l1_loss = (1.0 / total_pixels_masked) * torch.sum(
                    torch.abs(
                        torch.sub(cropped_labels_masked, corrected_cropped_predictions)
                    ), dim=[1, 2, 3])
                x.append(l1_loss)
        x = torch.stack(x)
        min_loss = torch.min(x, 0)[0]
        return torch.mean(min_loss)


class cPSNR(SRLoss):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.loss = MSE()

    def _forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor = None, **kwargs):
        if hr_mask is None:
            return self.unmasked_forward(sr, hr)
        return self.masked_forward(sr, hr, hr_mask)

    def new_forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor = None):
        height, width = hr.shape[-2:]
        if hr_mask is None:
            hr_mask = torch.ones_like(hr)
        border = 3
        max_pixel_shifts = 2 * border
        cropped_height, cropped_width = height - max_pixel_shifts, width - max_pixel_shifts
        sr_patch = sr[:, :, border:height - border, border:width - border]
        hr_patches = []
        mask_patches = []
        for i, j in product(range(max_pixel_shifts + 1), range(max_pixel_shifts + 1)):
            hr_patches.append(hr[:, :, i:i + cropped_height,
                              j:j + cropped_width])
            mask_patches.append(hr_mask[:, :, i:i + cropped_height,
                                j:j + cropped_width])

        hr_patches = torch.stack(hr_patches, dim=1)
        mask_patches = torch.stack(mask_patches, dim=1)
        sr_patch = torch.repeat_interleave(sr_patch.unsqueeze(1), hr_patches.shape[1], dim=1)
        total_unmasked = torch.sum(mask_patches, dim=[2, 3, 4]).view(-1, hr_patches.shape[1], 1, 1, 1)
        b = torch.pow(total_unmasked, -1) * torch.sum(hr_patches - sr_patch, dim=[2, 3, 4]).view(total_unmasked.shape)
        sr_patch = sr_patch + b
        loss_values = self.loss(sr_patch, hr_patches, mask_patches)
        loss_values = torch.mean(torch.min(loss_values, dim=1)[0])
        return loss_values

    def unmasked_forward(self, sr: torch.Tensor, hr: torch.Tensor):
        hr_size = hr.shape[-1]
        border = 3
        max_pixel_shifts = 2 * border
        size_cropped_image = hr_size - max_pixel_shifts
        cropped_predictions = sr[:, :, border:hr_size - border, border:hr_size - border]
        x = []

        for i in range(max_pixel_shifts + 1):
            for j in range(max_pixel_shifts + 1):
                cropped_labels = hr[:, :, i:i + size_cropped_image,
                                 j:j + size_cropped_image]
                total_pixels = cropped_labels.numel()
                b = (1.0 / total_pixels) * torch.sum(torch.sub(cropped_labels, cropped_predictions), dim=[1, 2, 3])
                b = b.view(-1, 1, 1, 1)
                corrected_cropped_predictions = cropped_predictions + b
                corrected_mse = (1.0 / total_pixels) * torch.sum(
                    torch.square(
                        torch.sub(cropped_labels, corrected_cropped_predictions)
                    ), dim=[1, 2, 3])
                x.append(corrected_mse)
        x = torch.stack(x)
        min_cmse = torch.min(x, 0)[0]
        cpsnr = -10 * torch.log10(min_cmse)
        return torch.mean(cpsnr)

    def masked_forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor):
        hr_size = hr.shape[-1]
        border = 3
        max_pixel_shifts = 2 * border
        crop_size = hr_size - max_pixel_shifts
        sr = sr * hr_mask
        cropped_predictions = (sr[:, :, border:hr_size - border, border:hr_size - border]).type(torch.float32)
        hr = (hr * hr_mask).type(torch.float32)
        x = []

        for i in range(max_pixel_shifts + 1):
            for j in range(max_pixel_shifts + 1):
                cropped_labels = hr[:, :, i:i + crop_size,
                                 j:j + crop_size]
                cropped_hr_mask = hr_mask[:, :, i:i + crop_size,
                                  j:j + crop_size].type(torch.float32)
                total_pixels_masked = torch.sum(cropped_hr_mask, dim=[1, 2, 3])
                b = (1.0 / total_pixels_masked) * torch.sum(torch.sub(cropped_labels,
                                                                      cropped_predictions), dim=[1, 2, 3])
                b = b.view(-1, 1, 1, 1)
                corrected_cropped_predictions = (cropped_predictions + b) * cropped_hr_mask
                corrected_mse = (1.0 / total_pixels_masked) * torch.sum(
                    torch.square(
                        torch.sub(cropped_labels, corrected_cropped_predictions)
                    ), dim=[1, 2, 3])
                x.append(corrected_mse)
        x = torch.stack(x)
        min_cmse = torch.min(x, 0)[0]
        cpsnr = -10 * torch.log10(min_cmse)
        return torch.mean(cpsnr)
