import torch
from superdeep.training import loss
from superdeep.data import SREntry
import torch.nn.functional as F
import lpips
from typing import Union, Tuple


class PSNR(loss.MSE):
    @property
    def best_min(self) -> bool:
        return False

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor = None, **kwargs):
        return -10 * torch.log10(super()._forward(sr, hr, hr_mask, ))


class cPSNR(loss.cMSE):
    @property
    def best_min(self) -> bool:
        return False

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor = None, **kwargs):
        return -10 * torch.log10(super()._forward(sr, hr, hr_mask, ))


class SAM(loss.SRLoss):
    @property
    def best_min(self) -> bool:
        return True

    @property
    def is_spectral(self):
        return True

    def __init__(self, calculate_for: str = 'all_lrs', shape: Union[str, Tuple] = 'max_lr',
                 register_images: bool = False, reduce_map: str = 'mean', reduce_lrs: str = None,
                 map_reduction_first: bool = True, **kwargs):
        """
        Spectral angle mapper loss
        :param calculate_for: 'lr_median', 'lr_mean', 'all_lrs', 'hr'
        :param shape: 'min_lr', 'max_lr', 'min_sr', 'max_sr', 'max_hr', 'min_hr', (h, w)
        :param register_images: coregister images before calculating SAM
        :param reduce_map: 'mean', 'median'
        :param reduce_lrs: 'mean', 'median', 'min'
        :param map_reduction_first: if True, reduce map first, then reduce lrs
        """
        super().__init__(**kwargs)
        self.calculate_for = calculate_for
        self.reduce_lrs = reduce_lrs

        # self.forward_func = {'lr_median': self.forward_median,
        #                      'best_lr': self.forward_best,
        #                      'hr': self.forward_hr}[calculate_for]

        self.shape_criterion = shape
        self.get_shape = self.get_shape_func()
        self.register_images = register_images
        self.reduce_map = reduce_map
        self.mode = 'bicubic'
        self.map_reduction_first = map_reduction_first
        self._lr_reduce_func = self._get_lr_reduce_func()
        self._map_reduce_func = self._get_map_reduce_func()

    def forward(self, sr: dict, hr: dict, hr_mask=None, lr_images: dict = None, **kwargs):
        return self._forward_func(sr, hr, hr_mask=hr_mask, lr_images=lr_images, **kwargs)

    def get_shape_func(self):
        shape = self.shape_criterion
        if shape == 'min_lr':
            def func(lrs, hrs, srs):
                return min([x.shape[-2:] for x in lrs.values()])
        elif shape == 'max_lr':
            def func(lrs, hrs, srs):
                return max([x.shape[-2:] for x in lrs.values()])
        elif shape == 'min_sr':
            def func(lrs, hrs, srs):
                return min([x.shape[-2:] for x in srs.values()])
        elif shape == 'max_sr':
            def func(lrs, hrs, srs):
                return max([x.shape[-2:] for x in srs.values()])
        elif shape == 'max_hr':
            def func(lrs, hrs, srs):
                return max([x.shape[-2:] for x in hrs.values()])
        elif shape == 'min_hr':
            def func(lrs, hrs, srs):
                return min([x.shape[-2:] for x in hrs.values()])
        elif isinstance(shape, tuple):
            def func(lrs, hrs, srs):
                return shape
        else:
            raise ValueError(f'Unknown shape criterion: {shape}')
        return func

    def _prepare_ref(self, hr: dict, lrs: dict, shape: tuple, keys: list):
        images = {}
        if self.calculate_for == 'lr_median':
            for k in keys:
                images[k] = F.interpolate(lrs[k], shape, mode=self.mode)
                images[k] = torch.median(lrs[k], dim=1, keepdim=True)
        elif self.calculate_for == 'lr_mean':
            for k in keys:
                images[k] = F.interpolate(lrs[k], shape, mode=self.mode)
                images[k] = torch.mean(lrs[k], dim=1, keepdim=True)
        elif self.calculate_for == 'all_lrs':
            for k in keys:
                images[k] = F.interpolate(lrs[k], shape, mode=self.mode).unsqueeze(1)
        elif self.calculate_for == 'hr':
            for k in keys:
                images[k] = F.interpolate(hr[k], shape, mode=self.mode)
        else:
            raise ValueError(f'Unknown calculate_for parameter: {self.calculate_for}')
        images = torch.cat([images[band] for band in keys], dim=1)
        if images.ndim == 4:
            images = images.unsqueeze(2)
        return images

    def _get_map_reduce_func(self):
        def mean_reduce(angles):
            return torch.mean(angles, dim=[-1, -2])

        def median_reduce(angles):
            return angles.view(*angles.shape[:-2], -1).median(dim=-1)[0]

        if self.reduce_map == 'mean':
            return mean_reduce
        elif self.reduce_map == 'median':
            return median_reduce
        else:
            raise ValueError(f'Unknown map reduce method: {self.reduce_map}')

    def _get_lr_reduce_func(self):
        def mean_reduce(angles):
            return torch.mean(angles, dim=1)

        def median_reduce(angles):
            return angles.median(dim=1)[0]

        def min_reduce(angles):
            return angles.min(dim=1)[0]

        if self.reduce_lrs == 'mean':
            return mean_reduce
        elif self.reduce_lrs == 'median':
            return median_reduce
        elif self.reduce_lrs == 'min':
            return min_reduce
        elif self.reduce_lrs is None:
            return lambda x: x[:, 0]
        else:
            raise ValueError(f'Unknown lr reduce method: {self.reduce_map}')

    def _register_images(self, sr: torch.Tensor, ref: torch.Tensor):
        from skimage.registration import phase_cross_correlation
        from scipy.ndimage import shift
        import time
        start = time.time()
        target = sr.cpu().numpy()
        reference = ref.cpu().numpy()
        for example_it in range(sr.shape[0]):
            for band_it in range(sr.shape[1]):
                for lr_it in range(sr.shape[2]):
                    translations = phase_cross_correlation(reference[example_it, band_it, lr_it],
                                                           target[example_it, band_it, lr_it],
                                                           return_error=False, upsample_factor=100)
                    target[example_it, band_it, lr_it] = shift(target[example_it, band_it, lr_it],
                                                               translations)
        end = time.time()
        print(f'Phase correlation took {end - start} seconds')
        return torch.from_numpy(target).to(sr.device)

    def _calculate_score(self, sr: torch.Tensor, ref: torch.Tensor, mask: torch.Tensor = None):
        #TODO: add mask support for examples across the batch. Best case scnenario is to
        # calculate the score for each example in the batch and then average it not by zeroing masked pixels
        # but removing them from calculations
        if mask is None:
            mask = sr.new_ones(sr.shape, dtype=torch.bool)
        mask = mask.sum(dim=1, keepdim=True).bool()[0,0]
        masked_ref, masked_sr = ref[:, :, mask], sr[:, :, mask]
        b = (1.0 / mask.sum(dim=[-2, -1])) * torch.sum(torch.sub(masked_ref, masked_sr), dim=-1, keepdim=True)
        masked_sr = masked_sr + b
        sr_norm = torch.sqrt(torch.sum(masked_sr * masked_sr, dim=1, keepdim=True))
        ref_norm = torch.sqrt(torch.sum(masked_ref * masked_ref, dim=1, keepdim=True))
        nominator = torch.sum(masked_ref * masked_sr, dim=1, keepdim=True)
        angle = torch.acos((nominator / (sr_norm * ref_norm + torch.finfo(torch.float64).eps)).clamp(-1.0, 1.0))
        result = torch.zeros(sr.shape[0], 1, *sr.shape[-2:], device=sr.device, dtype=torch.float64)
        result[:, :, mask] = angle
        return result


    def _forward_func(self, sr: dict, hr: dict, lr_images: dict, hr_mask: dict = None, **kwargs):
        target_shape = self.get_shape(lr_images, hr, sr)
        if hr_mask is None:
            hr_mask = {k: hr[k].new_ones((1, 1, *target_shape)) for k in hr.keys()}
        downscaled_predictions = {}
        for k, v in sr.items():
            downscaled_predictions[k] = F.interpolate(v, target_shape, mode=self.mode)
        keys = sorted(list(downscaled_predictions.keys()))
        downscaled_predictions = torch.cat([downscaled_predictions[band] for band in keys], dim=1)

        ref = self._prepare_ref(hr, lr_images, target_shape, keys)

        ref_mask = {}
        for k, v in hr_mask.items():
            ref_mask[k] = F.interpolate(v.to(torch.float16), target_shape, mode='nearest')
        ref_mask = torch.cat([ref_mask[band] for band in keys], dim=1).sum(dim=1, keepdim=True).bool()
        ref_mask = ref_mask.expand(-1, ref.shape[1], -1, -1)

        if self.register_images:
            downscaled_predictions = self._register_images(downscaled_predictions, ref)
        angles = []
        for i in range(ref.shape[2]):
            angles.append(self._calculate_score(downscaled_predictions, ref[:, :, i, :, :],
                                                ref_mask))
        angles = torch.cat(angles, dim=1)
        if self.map_reduction_first:
            angles = self._map_reduce_func(angles)
            angles = self._lr_reduce_func(angles)
        else:
            angles = self._lr_reduce_func(angles)
            angles = self._map_reduce_func(angles)
        return angles

    def forward_median(self, sr: dict, hr: dict, hr_mask=None, lr_images: dict = None, **kwargs):
        max_lr_shape = self.get_shape(lr_images, hr, sr)
        downscaled_predictions = {}
        for k, v in sr.items():
            downscaled_predictions[k] = F.interpolate(v, max_lr_shape, mode=mode)
        keys = sorted(list(downscaled_predictions.keys()))
        downscaled_predictions = torch.cat([downscaled_predictions[band] for
                                            band in keys], dim=1)
        lrs = {}
        for k in sr.keys():
            lrs[k] = F.interpolate(lr_images[k], max_lr_shape, mode=mode)
            lrs[k] = self.lr_reduce_func(lrs[k], dim=1, keepdim=True)
        lrs = torch.cat([lrs[band] for band in keys], dim=1)
        b = (1.0 / (lrs.shape[-1] * lrs.shape[-2])) * torch.sum(torch.sub(lrs, downscaled_predictions),
                                                                dim=[2, 3]).unsqueeze(-1).unsqueeze(-1)
        downscaled_predictions = downscaled_predictions + b
        sr_norm = torch.sqrt(torch.sum(downscaled_predictions * downscaled_predictions, dim=1, keepdim=True))
        lr_norm = torch.sqrt(torch.sum(lrs * lrs, dim=1, keepdim=True))
        nominator = torch.sum(lrs * downscaled_predictions, dim=1, keepdim=True)
        angle = torch.acos((nominator / (sr_norm * lr_norm)).clamp(-1.0, 1.0))
        if self.reduce_map not in [None, False]:
            if self.reduce_map == 'mean':
                angle = angle.mean(dim=[-1, -2])
            elif self.reduce_map == 'median':
                angle = angle.median(dim=[-1, -2])
            else:
                raise ValueError('Unknown reduce_map')
        return angle

    def forward_hr(self, sr: dict, hr: dict, hr_mask=None, lr_images: dict = None, **kwargs):
        mode = 'bicubic'
        max_lr_shape = self.get_shape(lr_images, hr, sr)
        downscaled_predictions = {}
        for k, v in sr.items():
            downscaled_predictions[k] = F.interpolate(v, max_lr_shape, mode=mode)
        keys = sorted(list(downscaled_predictions.keys()))
        downscaled_predictions = torch.cat([downscaled_predictions[band] for
                                            band in keys], dim=1)
        hrs = {}
        for k in sr.keys():
            hrs[k] = F.interpolate(hr[k], max_lr_shape, mode=mode)
        hrs = torch.cat([hrs[band] for band in keys], dim=1)
        b = (1.0 / (hrs.shape[-1] * hrs.shape[-2])) * torch.sum(torch.sub(hrs, downscaled_predictions),
                                                                dim=[2, 3]).unsqueeze(-1).unsqueeze(-1)
        downscaled_predictions = downscaled_predictions + b
        sr_norm = torch.sqrt(torch.sum(downscaled_predictions * downscaled_predictions, dim=1, keepdim=True))
        lr_norm = torch.sqrt(torch.sum(hrs * hrs, dim=1, keepdim=True))
        nominator = torch.sum(hrs * downscaled_predictions, dim=1, keepdim=True)
        angle = torch.acos((nominator / (sr_norm * lr_norm)).clamp(-1.0, 1.0))
        if self.reduce_map not in [None, False]:
            if self.reduce_map == 'mean':
                angle = angle.mean(dim=[-1, -2])
            elif self.reduce_map == 'median':
                angle = angle.median(dim=[-1, -2])
            else:
                raise ValueError('Unknown reduce_map')
        return angle

    def forward_best(self, sr: dict, hr: dict, hr_mask=None, lr_images: dict = None, **kwargs):
        mode = 'bicubic'
        max_lr_shape = self.get_shape(lr_images, hr, sr)
        # max_lr_shape = (864, 864)
        lrs_count = list(lr_images.values())[0].shape[1]
        downscaled_preds = {}
        for k, v in sr.items():
            downscaled_preds[k] = F.interpolate(v, max_lr_shape, mode=mode)
        keys = sorted(list(downscaled_preds.keys()))
        downscaled_preds = torch.cat([downscaled_preds[band] for
                                      band in keys], dim=1)
        results = []
        for i in range(lrs_count):
            lrs = dict()
            downscaled_predictions = torch.clone(downscaled_preds)
            for k in sr.keys():
                lrs[k] = F.interpolate(lr_images[k][:, i:i + 1, :, :], max_lr_shape, mode=mode)
            lrs = torch.cat([lrs[band] for band in keys], dim=1)
            b = (1.0 / (lrs.shape[-1] * lrs.shape[-2])) * torch.sum(torch.sub(lrs, downscaled_predictions),
                                                                    dim=[2, 3]).unsqueeze(-1).unsqueeze(-1)
            downscaled_predictions = downscaled_predictions + b
            sr_norm = torch.sqrt(torch.sum(downscaled_predictions * downscaled_predictions, dim=1))
            lr_norm = torch.sqrt(torch.sum(lrs * lrs, dim=1))
            nominator = torch.sum(lrs * downscaled_predictions, dim=1)
            angle = torch.acos((nominator / (sr_norm * lr_norm)).clamp(0., 1.0))
            results.append(angle)
        angles = torch.stack(results, dim=1)

        if self.reduce_map not in [None, False]:
            if self.reduce_map == 'mean':
                angle = angles.mean(dim=[-1, -2])
            elif self.reduce_map == 'median':
                angle = angles.median(dim=[-1, -2])
            elif self.reduce_map == 'inverse_median':
                angle = angles.min(dim=1)[0].median(dim=[-1, -2])
            elif self.reduce_map == 'inverse_mean':
                angle = angles.min(dim=1)[0].mean(dim=[-1, -2])
            else:
                raise ValueError('Unknown reduce_map')
        if angles.shape[1] > 1:
            angle = angles.min(dim=1)[0]
        return angle


class LPIPS(loss.SRLoss):
    @property
    def best_min(self) -> bool:
        return True

    def __init__(self, net='alex', device='cuda'):
        super().__init__()
        self.net = net
        self.device = device
        self.loss = lpips.LPIPS(net=net).to(device)

    def _prepare_input_tensor(self, img):
        img_interp = img * 2.0 - 1.0
        return img_interp.to(self.device).float()

    def _forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor = None, **kwargs):
        sr_tensor = self._prepare_input_tensor(sr)
        hr_tensor = self._prepare_input_tensor(hr)
        result = self.loss(sr_tensor, hr_tensor)
        return result


class SSIM(loss.SRLoss):
    @property
    def best_min(self) -> bool:
        return False

    def __init__(self, window_size=11, size_average=True):
        super(SSIM, self).__init__()
        self.window_size = window_size
        self.size_average = size_average
        self.channel = 1
        self.window = self.create_window(window_size, self.channel)

    @staticmethod
    def gaussian(window_size, sigma):
        from math import exp
        gauss = torch.Tensor([exp(-(x - window_size // 2) ** 2 / float(2 * sigma ** 2)) for x in range(window_size)])
        return gauss / gauss.sum()

    @staticmethod
    def create_window(window_size, channel):
        from torch.autograd import Variable
        _1D_window = SSIM.gaussian(window_size, 1.5).unsqueeze(1)
        _2D_window = _1D_window.mm(_1D_window.t()).float().unsqueeze(0).unsqueeze(0)
        window = Variable(_2D_window.expand(channel, 1, window_size, window_size).contiguous())
        return window

    @staticmethod
    def _ssim(img1, img2, window, window_size, channel, size_average=True):
        mu1 = F.conv2d(img1, window, padding=window_size // 2, groups=channel)
        mu2 = F.conv2d(img2, window, padding=window_size // 2, groups=channel)

        mu1_sq = mu1.pow(2)
        mu2_sq = mu2.pow(2)
        mu1_mu2 = mu1 * mu2

        sigma1_sq = F.conv2d(img1 * img1, window, padding=window_size // 2, groups=channel) - mu1_sq
        sigma2_sq = F.conv2d(img2 * img2, window, padding=window_size // 2, groups=channel) - mu2_sq
        sigma12 = F.conv2d(img1 * img2, window, padding=window_size // 2, groups=channel) - mu1_mu2

        C1 = 0.01 ** 2
        C2 = 0.03 ** 2

        ssim_map = ((2 * mu1_mu2 + C1) * (2 * sigma12 + C2)) / ((mu1_sq + mu2_sq + C1) * (sigma1_sq + sigma2_sq + C2))

        return ssim_map

    def _forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor = None, **kwargs):
        input_shape = sr.shape
        sr, hr, _ = self.mask_pixels(sr, hr, hr_mask)
        sr = sr.view(-1, *input_shape[-3:])
        hr = hr.view(-1, *input_shape[-3:])
        (_, channel, _, _) = sr.size()

        if channel == self.channel and self.window.data.type() == sr.data.type():
            window = self.window
        else:
            window = self.create_window(self.window_size, channel)

            # if sr.is_cuda:
            #     window = window.cuda(sr.get_device())
            window = window.type_as(sr)

            self.window = window
            self.channel = channel
        result = self._ssim(sr, hr, window, self.window_size, channel, self.size_average).mean((-3, -2, -1))
        result = result.view(input_shape[:-3])
        return result


class cSSIM(loss.SRLoss):
    @property
    def best_min(self) -> bool:
        return True

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.loss = SSIM()

    def _forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor = None, **kwargs):
        if hr_mask is None:
            return self.unmasked_forward(sr, hr)
        return self.masked_forward(sr, hr, hr_mask)

    def unmasked_forward(self, sr: torch.Tensor, hr: torch.Tensor):
        hr_size = hr.shape[-2:]
        border = 3
        max_pixel_shifts = 2 * border
        size_cropped_image = (hr_size[0] - max_pixel_shifts, hr_size[1] - max_pixel_shifts)
        cropped_predictions = sr[:, :, border:hr_size[0] - border, border:hr_size[1] - border]
        x = []

        for i in range(max_pixel_shifts + 1):
            for j in range(max_pixel_shifts + 1):
                cropped_labels = hr[:, :, i:i + size_cropped_image[0],
                                 j:j + size_cropped_image[1]]
                total_pixels = cropped_labels.numel()
                b = (1.0 / total_pixels) * torch.sum(torch.sub(cropped_labels, cropped_predictions), dim=[1, 2, 3])
                b = b.view(-1, 1, 1, 1)
                corrected_cropped_predictions = cropped_predictions + b
                corrected_mse = self.loss(corrected_cropped_predictions, cropped_labels)
                x.append(corrected_mse)
        x = torch.stack(x)
        min_cmse = torch.max(x, 0)[0]
        return torch.mean(min_cmse)

    def masked_forward(self, sr: torch.Tensor, hr: torch.Tensor, hr_mask: torch.Tensor):
        hr_size = hr.shape[-2:]
        border = 3
        max_pixel_shifts = 2 * border
        size_cropped_image = (hr_size[0] - max_pixel_shifts, hr_size[1] - max_pixel_shifts)
        cropped_predictions = sr[:, :, border:hr_size[0] - border, border:hr_size[1] - border]
        x = []

        for i in range(max_pixel_shifts + 1):
            for j in range(max_pixel_shifts + 1):
                cropped_labels = hr[:, :, i:i + size_cropped_image[0],
                                 j:j + size_cropped_image[1]]
                cropped_hr_mask = hr_mask[:, :, i:i + size_cropped_image[0],
                                  j:j + size_cropped_image[1]].type(torch.float32)
                cropped_labels = cropped_labels * cropped_hr_mask
                masked_cropped_predictions = cropped_predictions * cropped_hr_mask
                total_pixels_masked = torch.sum(cropped_hr_mask, dim=[1, 2, 3])
                b = (1.0 / total_pixels_masked) * torch.sum(torch.sub(cropped_labels,
                                                                      masked_cropped_predictions), dim=[1, 2, 3])
                b = b.view(-1, 1, 1, 1)
                corrected_cropped_predictions = (masked_cropped_predictions + b) * cropped_hr_mask
                corrected_mse = self.loss(cropped_labels, corrected_cropped_predictions)
                x.append(corrected_mse)
        x = torch.stack(x)
        min_cmse = torch.max(x, 0)[0]
        return torch.mean(min_cmse)
