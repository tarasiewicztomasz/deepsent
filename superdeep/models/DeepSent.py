import math
import torch
from torch import nn
from torch.nn import functional as F
from superdeep import constants, data
from superdeep.models import MultiBandModel


def _next_pow_2(x: int):
    return int(pow(2, math.ceil(math.log2(x))))

class Encoder(nn.Module):
    def __init__(self, in_channels: int = 2, num_layers: int = 2, kernel_size: int = 3, channel_size: int = 64):
        """
        Args:
            config : dict, configuration file
        """

        super(Encoder, self).__init__()
        in_channels = in_channels
        num_layers = num_layers
        kernel_size = kernel_size
        channel_size = channel_size
        padding = kernel_size // 2

        self.init_layer = nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=channel_size, kernel_size=kernel_size, padding=padding),
            nn.PReLU())

        res_layers = [ResidualBlock(channel_size, kernel_size) for _ in range(num_layers)]
        self.res_layers = nn.Sequential(*res_layers)

        self.final = nn.Sequential(
            nn.Conv2d(in_channels=channel_size, out_channels=channel_size, kernel_size=kernel_size, padding=padding)
        )

    def forward(self, x):
        """
        Encodes an input tensor x.
        Args:
            x : tensor (B, C_in, W, H), input images
        Returns:
            out: tensor (B, C, W, H), hidden states
        """
        x = self.init_layer(x)
        x = self.res_layers(x)
        x = self.final(x)
        return x


class RecursiveNet(nn.Module):

    def __init__(self, in_channels: int = 64, num_layers: int = 2, kernel_size: int = 3, alpha_residual: bool = True):
        """
        Args:
            config : dict, configuration file
        """

        super(RecursiveNet, self).__init__()

        self.input_channels = in_channels
        self.num_layers = num_layers
        self.alpha_residual = alpha_residual
        kernel_size = kernel_size
        padding = kernel_size // 2

        self.fuse = nn.Sequential(
            ResidualBlock(2 * self.input_channels, kernel_size),
            nn.Conv2d(in_channels=2 * self.input_channels, out_channels=self.input_channels,
                      kernel_size=kernel_size, padding=padding),
            nn.PReLU())

    def forward(self, x, alphas=None):
        """
        Fuses hidden states recursively.
        Args:
            x : tensor (B, L, C, W, H), hidden states
            alphas : tensor (B, L, 1, 1, 1), boolean indicator (0 if padded low-res view, 1 otherwise)
        Returns:
            out: tensor (B, C, W, H), fused hidden state
        """
        if isinstance(x, tuple):
            alphas = x[-1]
            x = x[0]
        batch_size, nviews, channels, width, heigth = x.shape
        parity = nviews % 2
        half_len = nviews // 2
        while half_len > 0:
            alice = x[:, :half_len]  # first half hidden states (B, L/2, C, W, H)
            bob = x[:, half_len:nviews - parity]  # second half hidden states (B, L/2, C, W, H)
            bob = torch.flip(bob, [1])

            alice_and_bob = torch.cat([alice, bob], 2)  # concat hidden states accross channels (B, L/2, 2*C, W, H)
            alice_and_bob = alice_and_bob.view(-1, 2 * channels, width, heigth)
            x = self.fuse(alice_and_bob)
            x = x.view(batch_size, half_len, channels, width, heigth)  # new hidden states (B, L/2, C, W, H)

            if self.alpha_residual:  # skip connect padded views (alphas_bob = 0)
                alphas_alice = alphas[:, :half_len]
                alphas_bob = alphas[:, half_len:nviews - parity]
                alphas_bob = torch.flip(alphas_bob, [1])
                x = alice + alphas_bob * x
                alphas = alphas_alice

            nviews = half_len
            parity = nviews % 2
            half_len = nviews // 2

        return torch.mean(x, 1)


class Decoder(nn.Module):
    def __init__(self, in_channels: int = 64, out_channels: int = 64, kernel_size: int = 3, stride: int = 3,
                 final_in_channels: int = 64, final_out_channels: int = 1, final_kernel_size: int = 1):
        """
        Args:
            config : dict, configuration file
        """

        super(Decoder, self).__init__()

        self.deconv = nn.Sequential(nn.ConvTranspose2d(in_channels=in_channels,
                                                       out_channels=out_channels,
                                                       kernel_size=kernel_size,
                                                       stride=stride),
                                    nn.PReLU())

        self.final = nn.Conv2d(in_channels=final_in_channels,
                               out_channels=final_out_channels,
                               kernel_size=final_kernel_size,
                               padding=final_kernel_size // 2)

    def forward(self, x):
        """
        Decodes a hidden state x.
        Args:
            x : tensor (B, C, W, H), hidden states
        Returns:
            out: tensor (B, C_out, 3*W, 3*H), fused hidden state
        """

        x = self.deconv(x)
        x = self.final(x)
        return x

class ResidualBlock(nn.Module):
    def __init__(self, channel_size: int = 64, kernel_size: int = 3):
        """
        Args:
            channel_size : int, number of hidden channels
            kernel_size : int, shape of a 2D kernel
        """

        super(ResidualBlock, self).__init__()
        padding = kernel_size // 2
        self.block = nn.Sequential(
            nn.Conv2d(in_channels=channel_size, out_channels=channel_size, kernel_size=kernel_size, padding=padding),
            nn.PReLU(),
            nn.Conv2d(in_channels=channel_size, out_channels=channel_size, kernel_size=kernel_size, padding=padding),
            nn.PReLU()
        )

    def forward(self, x):
        """
        Args:
            x : tensor (B, C, W, H), hidden state
        Returns:
            x + residual: tensor (B, C, W, H), new hidden state
        """

        residual = self.block(x)
        return x + residual

class ShuffleDecoder(nn.Module):
    def __init__(self, upscale_factor: int, in_channels: int = 64, out_channels: int = 64, kernel_size: int = 3,
                 stride: int = 1, final_in_channels: int = 64, final_out_channels: int = 1, final_kernel_size: int = 1):
        """
        Args:
            config : dict, configuration file
        """

        super(ShuffleDecoder, self).__init__()
        self.scale = upscale_factor
        self.deconv = nn.Sequential(nn.Conv2d(in_channels=in_channels,
                                              out_channels=out_channels * upscale_factor * upscale_factor,
                                              kernel_size=kernel_size,
                                              stride=stride,
                                              padding=kernel_size // 2),
                                    nn.PReLU())

        self.final = nn.Conv2d(in_channels=in_channels,
                               out_channels=final_out_channels,
                               kernel_size=final_kernel_size,
                               padding=final_kernel_size // 2)

    def forward(self, x):
        """
        Decodes a hidden state x.
        Args:
            x : tensor (B, C, W, H), hidden states
        Returns:
            out: tensor (B, C_out, 3*W, 3*H), fused hidden state
        """

        x = self.deconv(x)
        x = F.pixel_shuffle(x, self.scale)
        x = self.final(x)
        return x


class ShuffleDecoderInstanceNorm(nn.Module):
    def __init__(self, upscale_factor: int, in_channels: int = 64, out_channels: int = 64, kernel_size: int = 3,
                 stride: int = 1, final_in_channels: int = 64, final_out_channels: int = 1, final_kernel_size: int = 1):
        """
        Args:
            config : dict, configuration file
        """

        super(ShuffleDecoderInstanceNorm, self).__init__()
        self.scale = upscale_factor
        self.deconv = nn.Sequential(nn.Conv2d(in_channels=in_channels,
                                              out_channels=out_channels,
                                              kernel_size=kernel_size,
                                              stride=stride,
                                              padding=kernel_size // 2),
                                    nn.InstanceNorm2d(out_channels),
                                    nn.Conv2d(in_channels=in_channels,
                                              out_channels=out_channels * upscale_factor * upscale_factor,
                                              kernel_size=kernel_size,
                                              stride=stride,
                                              padding=kernel_size // 2),
                                    nn.PReLU())

        self.final = nn.Conv2d(in_channels=in_channels,
                               out_channels=final_out_channels,
                               kernel_size=final_kernel_size,
                               padding=final_kernel_size // 2)

    def forward(self, x):
        """
        Decodes a hidden state x.
        Args:
            x : tensor (B, C, W, H), hidden states
        Returns:
            out: tensor (B, C_out, s*W, s*H), fused hidden state
        """
        if x is None:
            return x
        x = self.deconv(x)
        x = F.pixel_shuffle(x, self.scale)
        x = self.final(x)
        return x

class DeepSent(MultiBandModel):
    def __init__(self, encoder: Encoder = Encoder, recursive_net: RecursiveNet = RecursiveNet, **kwargs):
        """
        Args:
            config : dict, configuration file
        """
        super(DeepSent, self).__init__()
        self.band_encoders = nn.ModuleDict({band: nn.Sequential(
            encoder(),
            nn.InstanceNorm2d(64)
        ) for band in constants.S2_BANDS})
        self.band_preshuffle = nn.ModuleDict(
            {band: nn.Sequential(nn.Conv2d(in_channels=64, out_channels=9, kernel_size=(3, 3), padding=1),
                                 nn.PReLU())
             for band in constants.S2_BANDS}
        )
        self.band_finals = nn.ModuleDict(
            {band: nn.Sequential(nn.Conv2d(in_channels=2, out_channels=1, kernel_size=(3, 3), padding=1),
                                 nn.PReLU())
             for band in constants.S2_BANDS}
        )

        self.lr_fuse = nn.ModuleDict({str(group): recursive_net() for group in constants.S2_BANDS})
        self.band_fuse = nn.ModuleDict({str(group): nn.Sequential(recursive_net(),
                                                                  nn.InstanceNorm2d(64))
                                        for group in constants.S2_RES_GROUPS.keys()})
        self.transconv10 = ShuffleDecoderInstanceNorm(upscale_factor=1, final_out_channels=64)
        self.transconv20 = ShuffleDecoderInstanceNorm(upscale_factor=2, final_out_channels=64)
        self.transconv60 = ShuffleDecoderInstanceNorm(upscale_factor=3, final_out_channels=64)
        self.fuse_10_20 = recursive_net(alpha_residual=False)
        self.fuse_20_60 = recursive_net(alpha_residual=False)
        self.upscaling_factors = {10: 3, 20: 6, 60: 18}
        self.band_norms = nn.ModuleDict({str(band): nn.InstanceNorm2d(64) for band in constants.S2_BANDS})

    def forward(self, entry: data.MultiBandSREntry, **kwargs):
        """
        Super resolves a batch of low-resolution images.
        Args:
            entry: collated MultiBandSREntry
            **kwargs: additional arguments
        Returns:
            srs: tensor (B, C_out, W, H) or of tensors, super-resolved images
        """
        output_bands = entry.output_bands
        band_lrs = entry.lr_images

        assert output_bands is not None, f"{self.__class__.__name__}: output_bands required in forward function!"
        x = {}
        fused_groups = {}
        for band_res, bands in constants.S2_RES_GROUPS.items():
            x[band_res] = {}
            for band, lrs in [(b, band_lrs[b]) for b in (set(band_lrs.keys()) & set(bands))]:
                batch_size, seq_len, height, width = lrs.shape
                next_pow = _next_pow_2(seq_len)
                lrs = lrs.view(-1, seq_len, 1, height, width)
                padded_lrs = torch.zeros(batch_size, next_pow - seq_len, 1, height, width, device=lrs.device)
                lrs = torch.cat([lrs, padded_lrs], 1)

                alphas = torch.ones(batch_size, seq_len, device=lrs.device)
                alphas = alphas.view(-1, seq_len, 1, 1, 1)
                non_alphas = torch.zeros(batch_size, next_pow - seq_len, 1, 1, 1, device=lrs.device)
                alphas = torch.cat([alphas, non_alphas], 1)

                refs, _ = torch.median(lrs[:, :seq_len], 1,
                                       keepdim=True)  # reference image aka anchor, shared across multiple views
                refs = refs.repeat(1, next_pow, 1, 1, 1)
                stacked_input = torch.cat([lrs, refs], 2)  # tensor (B, L, 2*C_in, W, H))

                stacked_input = stacked_input.view(batch_size * next_pow, 2, height, width)
                layer1 = self.band_encoders[band](stacked_input)  # encode input tensor
                layer1 = layer1.view(batch_size, next_pow, -1, height, width)  # tensor (B, L, C, W, H)
                recursive_layer = self.lr_fuse[str(band)](layer1, alphas=alphas)  # fuse hidden states (B, C, W, H)
                x[band_res][band] = self.band_norms[str(band)](recursive_layer)

            # band fusion
            if len(list(x[band_res].keys())) == 0:
                fused_groups[band_res] = None
                continue
            band_fusion = torch.stack([x[band_res][band] for band in sorted(list(x[band_res].keys()))], dim=1)
            batch_size, seq_len, channels, height, width = band_fusion.shape
            next_pow = _next_pow_2(seq_len)
            padded_band_fusion = torch.zeros(batch_size, next_pow - seq_len, channels, height, width,
                                             device=band_fusion.device)
            band_fusion = torch.cat([band_fusion, padded_band_fusion], 1)
            alphas = torch.ones(batch_size, seq_len, device=band_fusion.device)
            alphas = alphas.view(-1, seq_len, 1, 1, 1)
            non_alphas = torch.zeros(batch_size, next_pow - seq_len, 1, 1, 1, device=band_fusion.device)
            alphas = torch.cat([alphas, non_alphas], 1)
            fused_groups[band_res] = self.band_fuse[str(band_res)]((band_fusion, alphas))

        emb = fused_groups[60]
        emb = self.transconv60(emb)
        groups_to_merge = list(filter(lambda el: el is not None, [fused_groups[20], emb]))
        if len(groups_to_merge) > 0:
            if len(groups_to_merge) == 1:
                groups_to_merge.append(groups_to_merge[0])
            emb = torch.stack(groups_to_merge, dim=1)
            emb = self.fuse_20_60(emb)
        emb = self.transconv20(emb)
        groups_to_merge = list(filter(lambda el: el is not None, [fused_groups[10], emb]))
        if len(groups_to_merge) > 0:
            if len(groups_to_merge) == 1:
                groups_to_merge.append(groups_to_merge[0])
            emb = torch.stack(groups_to_merge, dim=1)
            emb = self.fuse_10_20(emb)
        emb = self.transconv10(emb)

        outputs = {}
        for output_band in output_bands:
            resolution = constants.S2_BANDS_RES[output_band]
            upscaled = F.interpolate(band_lrs[output_band],
                                     scale_factor=self.upscaling_factors[resolution], mode='bicubic')
            upscaled = torch.mean(upscaled, 1, keepdim=True)
            outputs[output_band] = self.band_preshuffle[output_band](emb)
            outputs[output_band] = torch.cat([F.pixel_shuffle(outputs[output_band], 3), upscaled], dim=1)
            outputs[output_band] = self.band_finals[output_band](outputs[output_band])
        return outputs