from superdeep.models import SRModel
import torch.nn.functional as F
from superdeep.data.entry import MultiBandSREntry
from typing import MutableMapping

class Bicubic(SRModel):
    """
    The model returning bicubically interpolated image by upscaling each
    input image and taking a mean value for each pixel.
    """
    def __init__(self, scale=None, **kwargs):
        super().__init__()
        self.scale = scale

    def forward(self, entry):
        scale = self.scale
        if isinstance(entry, MultiBandSREntry):
            if not isinstance(scale, MutableMapping):
                scale = {band: scale for band in entry.bands}
            return {band: self._single_band_forward(entry[band], scale[band]) for band in entry.bands}
        return self._single_band_forward(entry, scale)
    def _single_band_forward(self, entry, scale):
        x = entry.lr_images
        if scale is None:
            assert entry.hr_image is not None, f"Scale is not specified and HR image is" \
                                               f" not provided for band {entry.band}"
            scale = round(entry.hr_image.shape[-1] / x.shape[-1])
        img = x.new_zeros((1, 1, x.shape[-2] * scale, x.shape[-1] * scale))
        for i in range(x.shape[1]):
            example = x[0:1, i:i + 1, :, :]
            img += F.interpolate(example, scale_factor=scale, mode='bicubic', align_corners=False)
        img = img / x.shape[1]
        return img


class NearestNeighbors(SRModel):
    """
    The model returning image upscaled using nearest neighbors algorithm. It upscales each
    input image and taking a mean value for each pixel.
    """
    def __init__(self, scale=None, **kwargs):
        super().__init__()
        self.scale = scale

    def forward(self, entry):
        x = entry.lr_images
        if self.scale is not None:
            scale = self.scale
        else:
            if entry.hr_image is not None:
                scale = round(entry.hr_image.shape[-1] / x.shape[-1])
            else:
                raise Exception("Can't predict output shape of an image. Please ensure that scale is set or "
                                "HR image is not None.")
        img = x.new_zeros((1, 1, x.shape[-2] * scale, x.shape[-1] * scale))
        for i in range(x.shape[1]):
            example = x[0:1, i:i + 1, :, :]
            img += F.interpolate(example, scale_factor=scale, mode='nearest')
        img = img / x.shape[1]
        return img


class LRs(SRModel):
    """
    The model returning a set of images upscaled using nearest neighbors algorithm.
    """
    def __init__(self, scale=None, **kwargs):
        super().__init__()
        self.scale = scale

    def forward(self, entry):
        x = entry.lr_images
        if self.scale is not None:
            if self.scale == 1:
                return x
            scale = self.scale
        else:
            if entry.hr_image is not None:
                scale = round(entry.hr_image.shape[-1] / x.shape[-1])
            else:
                raise Exception("Can't predict output shape of an image. Please ensure that scale is set or "
                                "HR image is not None.")
        img = x.new_zeros((1, x.shape[-3], x.shape[-2] * scale, x.shape[-1] * scale))
        for i in range(x.shape[1]):
            img[0:1, i:i+1, :, :] = F.interpolate(x[0:1, i:i + 1, :, :], scale_factor=scale, mode='nearest')
        return img