from .base import MultiBandModel, SRModel
from .DeepSent import DeepSent
from .Classic import NearestNeighbors, Bicubic, LRs
from .RAMS import RAMS