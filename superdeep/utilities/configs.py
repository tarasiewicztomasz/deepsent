from dataclasses import dataclass, field
from typing import Union, Type, List, Callable, ClassVar
from collections.abc import Sequence
from torch import nn, optim
from superdeep.training import loss
from superdeep.data import collation, configurator
from superdeep import models, constants
import json
import inspect
import os
import time
from collections import namedtuple
from importlib import import_module
from omegaconf import ListConfig


ClassDescription = namedtuple('ClassDescription', ['module', 'name'])


def bands_to_str(bands: Union[str, List[str]]):

    if isinstance(bands, str):
        bands = [bands]
    if set(bands) == set(constants.S2_BANDS):
        return 'all'
    for k, v in constants.S2_RES_GROUPS.items():
        if set(bands) == set(v):
            return f"{k}m"
    return ''.join(sorted(bands))


def str_to_bands(bands: str):
    if isinstance(bands, list) or isinstance(bands, ListConfig):
        return list(bands)
    elif isinstance(bands, str):
        if bands == 'all':
            return constants.S2_BANDS
        for k in constants.S2_BANDS:
            if bands == k:
                return k
        splits = bands.split('+')
        print(splits)
        bands_list = []
        for split in splits:
            split = int(''.join(filter(str.isdigit, split)))
            if split in constants.S2_RES_GROUPS.keys():
                bands_list += constants.S2_RES_GROUPS[split]
        return sorted(bands_list)
    raise ValueError(f"Invalid bands: {bands}")


@dataclass
class Config:
    """
    Config class storing training configuration variables. Necessary to resume interrupted training process.

    :arg model: a class of the model to be trained
    :arg model_params: parameters required to instantiate the model
    :arg dataset: a DatasetConfigurator subclass defining input pipeline for the data
    :arg dataset_params: parameters to instantiate dataset configurator, may vary depending on the chosen subclass
    :arg input_bands: a list of input bands names. These bands will be loaded and passed to the model
    :arg output_bands: (optional) a list of output bands names. Useful if the training should be focused on specific bands e.g. DeepSent model needs all Sentinel-2 bands as an input but it does not have to super-resolve all of them
    :arg collate_function: a class responsible for collating input images depending on the problem (MISR or SISR)
    :arg train_loss: a loss function class used for training
    :arg test_loss: a loss function or metric class used for validation
    :arg optimizer: an optimizer class
    :arg learning_rate: initial learning rate of the optimizer
    :arg optimizer_params: additional arguments required to instantiate optimizer class
    :arg batch_size: choose how many images should be collated together during the training
    :arg patch_size: if not None the input training images are divided into patches of this size. WARNING: this value represents the largest possible HR and the LR patches are calculated based on HR-LR size ratio. The smaller images from different bands have this value scaled depending on their shape and the maximum shape on the input. For example, having 288x288 HR patches and 96x96 LR ones ofr 10m bands may result in 48x48 HR and 16x16 LR images for 60m bands.
    :arg patch_stride: stride for choosing the patches. Lower value than patch_size makes the patches overlapping on each other while higher value skips some pixels. Default: same as patch_size
    :arg single_output_training: randomly chooses the band for calculating the loss to save the GPU memory during the training, defualt False
    :arg register_lrs: not implemented yet
    """
    model: Type[nn.Module] = None
    dataset: Union[str, Type[configurator.DatasetConfigurator]] = None
    train_loss: Type[loss.SRLoss] = None
    test_loss: Type[loss.SRLoss] = None
    batch_size: int = None
    patch_size: Union[int, None] = None
    patch_stride: Union[int, None] = None
    input_bands: Union[str, List[str]] = None
    output_bands: Union[str, List[str], None] = None
    collate_function: Type[collation.Collation] = None
    last_epoch: int = 0
    best_epoch: int = 0
    best_loss: float = None
    learning_rate: float = 1e-4
    main_logdir: str = 'logs'
    model_name: str = None
    register_lrs: bool = False
    single_output_training: bool = False
    optimizer: Union[Type[optim.Optimizer], Callable] = None
    model_params: dict = field(default_factory=lambda: {})
    optim_params: dict = field(default_factory=lambda: {})
    dataset_params: dict = field(default_factory=lambda: {})
    other_params: dict = field(default_factory=lambda: {})
    FILENAME: ClassVar[str] = 'config.json'

    def __post_init__(self):
        if self.output_bands is None:
            self.output_bands = self.input_bands
        elif isinstance(self.output_bands, str):
            self.output_bands = [self.output_bands]
        if self.model_name is None:
            self.model_name = self.create_log_name
        if self.patch_stride is None:
            self.patch_stride = self.patch_size

    def to_dict(self):
        config_dict = {}
        for k, v in vars(self).items():
            if inspect.isclass(v):
                v = ClassDescription(module=v.__module__, name=v.__name__)._asdict()
            config_dict[k] = v
        return config_dict

    @property
    def logdir(self):
        if self.model_name is not None:
            return os.path.join(self.main_logdir, self.model_name)
        return os.path.join(self.main_logdir, self.create_log_name)

    @property
    def create_log_name(self):
        if self.model_name is not None:
            return self.model_name
        logdir_proto = [(None, self.model.__name__), ('p', self.patch_size), ('b', self.batch_size),
                        (None, self.train_loss.__name__), (None, self.dataset_name)]
        logdir = ''
        for i, (name, value) in enumerate(logdir_proto):
            if i > 0:
                logdir += '_'
            if name is not None:
                logdir += name
            logdir += str(value)
        if self.input_bands is not None:
            if self.input_bands == self.output_bands:
                input_bands = bands_to_str(self.input_bands)
                logdir += f'_b{input_bands}'
            else:
                input_bands = bands_to_str(self.input_bands)
                output_bands = bands_to_str(self.output_bands)
                logdir += f'_bin{input_bands}_bout{output_bands}'
        return logdir + '_' + time.strftime("%Y%m%d-%H%M%S")

    @classmethod
    def _bands_str_to_list(cls, bands: str):
        if bands == 'all':
            return constants.S2_BANDS
        elif bands == '10m':
            return constants.S2_RES_GROUPS[10]
        else:
            return bands

    @property
    def dataset_name(self):
        if isinstance(self.dataset, str):
            return self.dataset.split('\\')[-1]
        else:
            return self.dataset(input_bands=self.input_bands, **self.dataset_params).name

    def save(self):
        if not os.path.isdir(self.logdir):
            os.makedirs(self.logdir)
        with open(os.path.join(self.logdir, self.FILENAME), 'w') as file:
            file.write(json.dumps(self.to_dict(), indent=4))

    @classmethod
    def from_file(cls, logdir, filename=None):
        if filename is None:
            filename = cls.FILENAME
        config_path = os.path.join(logdir, filename)
        assert os.path.isfile(config_path), f"File doesn\'t exist: {config_path}"
        with open(config_path) as f:
            config_file = json.load(f)
        for k, v in config_file.items():
            if isinstance(v, dict) and set(ClassDescription._fields) <= set(v):
                try:
                    module = import_module(v.get('module'))
                    config_file[k] = getattr(module, v.get('name'))
                except ModuleNotFoundError as e:
                    print(f"Error when importing... {k}: {v}")
                    raise e
        config_file['input_bands'] = cls._bands_str_to_list(config_file['input_bands'])
        config_file['output_bands'] = cls._bands_str_to_list(config_file['output_bands'])
        config_file['model_name'] = logdir.split('\\')[-1]
        return cls.from_dict(config_file)

    @classmethod
    def from_logdir(cls, path):
        path_parts = path.split('\\')
        model_path = path_parts[-1]
        model, patch, batch, train_loss, dataset, *bands = tuple(model_path.split('_'))
        band_dict = {}
        for b in bands:
            if 'bout' in b:
                band_dict['output_bands'] = b[:3]
                continue
            band_dict['input_bands'] = b[:4]
        config = cls(
            model=getattr(models, model),
            patch_size=patch[1:],
            batch_size=batch[1:],
            train_loss=getattr(loss, train_loss),
            main_logdir='\\'.join(path_parts[:-1]) if len(path_parts) > 1 else '',
            model_name=path_parts[-1],
            # dataset=getattr(configurator, dataset),
            **band_dict
        )
        return config

    @classmethod
    def from_dict(cls, config_dict: dict):
        if 'model_logdir' in list(config_dict.keys()):
            name = config_dict.pop('model_logdir')
            config_dict['model_name'] = name
        return cls(**config_dict)
