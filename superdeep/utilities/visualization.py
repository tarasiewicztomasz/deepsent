from pathlib import Path
import math
import matplotlib.pyplot as plt
import numpy as np
import cv2
import torch
import torchvision.transforms.functional as F
from torchvision.utils import make_grid
from typing import List, Union, Iterable
from itertools import product
from skimage import exposure


class ImageVisualizer:
    def __call__(self, images):
        raise NotImplementedError


class GridPlotTorch(ImageVisualizer):
    def __init__(self, nrow: int = 8):
        super().__init__()
        self.nrow = nrow

    def show(self, images):
        if not isinstance(images, list):
            images = [images]
        fix, axs = plt.subplots(ncols=len(images), squeeze=False)
        for i, img in enumerate(images):
            img = img.detach()
            img = F.to_pil_image(img)
            axs[0, i].imshow(np.asarray(img))
            axs[0, i].set(xticklabels=[], yticklabels=[], xticks=[], yticks=[])
        plt.show()

    def __call__(self, images: Union[List[torch.Tensor], torch.Tensor]):
        grid = make_grid(images, nrow=self.nrow)
        self.show(grid)


class ModelComparisonVisualizer(ImageVisualizer):
    def __call__(self, image_list: List, labels: List = None):
        hr_index = 0
        if 'HR' in labels:
            hr_index = labels.index('HR')
        elif "LRs" in labels:
            hr_index = labels.index('LRs')
        size = math.ceil(len(image_list) ** 0.5)
        blank_images_count = size * size - len(image_list)
        images = []
        for i, image in enumerate(image_list):
            if isinstance(image, str):
                image = Path(image)
                if image.is_dir():
                    image = list(image.glob('*'))[0]
                else:
                    image = image.parent / f'{image.name}.png'
                image = cv2.imread(str(image), cv2.IMREAD_UNCHANGED)
            elif isinstance(image, torch.Tensor):
                image = image.detach().cpu().numpy()
            if i == hr_index:
                try:
                    image = hist_equal(np.clip(image / (2 ** 14 - 1), 0., 1.))
                except ValueError as e:
                    print(labels[i], image.min(), image.max())
                    plt.imshow(image)
                    plt.show()
                    raise e
            images.append(image)

        for i, image in enumerate(images):
            if i == hr_index:
                continue
            images[i] = hist_match(images[i] / (2 ** 14 - 1), images[hr_index])

        shapes = _get_shapes(images)
        if len(shapes) > 1:
            max_shape = max(shapes)
            images = _upscale_to_largest(images, max_shape)
        shapes = _get_shapes(images)
        assert len(shapes) == 1, f'Images of different sizes: {shapes}'
        images = _add_ids(images)
        images = _add_borders(images, 2)
        images += [np.ones_like(images[0])] * (blank_images_count % size)
        concatenated = []
        for row in range(size - blank_images_count // size):
            concatenated.append(images[row * size:row * size + size])
        concatenated = (_concat_vh(concatenated) * (2 ** 16 - 1)).astype(np.uint16)
        result = _add_model_names(concatenated, labels)
        return result


def _add_ids(images, font_type=cv2.FONT_HERSHEY_PLAIN):
    for i, image in enumerate(images, 1):
        font_size, text_size = _get_font_scale(image, str(i), height_coverage=0.15)
        cv2.putText(image, str(i), (0, image.shape[0]), font_type, font_size, 1, thickness=round(font_size))
    return images


def _upscale_to_largest(images, max_shape):
    for i, image in enumerate(images):
        images[i] = cv2.resize(image, (max_shape[1], max_shape[0]), 0, 0, cv2.INTER_NEAREST)
    return images


def _add_borders(images, border_size):
    for i, image in enumerate(images):
        images[i] = cv2.copyMakeBorder(image, *[border_size] * 4, cv2.BORDER_CONSTANT, None, value=1.0)
    return images


def _get_font_scale(img, text, height_coverage=1.0, font_type=cv2.FONT_HERSHEY_PLAIN):
    text_size = cv2.getTextSize(text, fontFace=font_type, fontScale=1, thickness=1)
    scale = img.shape[-2] / text_size[0][1]
    text_size = cv2.getTextSize(text, fontFace=font_type, fontScale=scale, thickness=round(scale))
    while text_size[0][1] > img.shape[-2]*height_coverage:
        scale = scale * 0.9
        text_size = cv2.getTextSize(text, fontFace=font_type, fontScale=scale, thickness=round(scale))
    while text_size[0][0] > img.shape[-1]:
        scale = scale * 0.9
        text_size = cv2.getTextSize(text, fontFace=font_type, fontScale=scale, thickness=round(scale))
    return scale, text_size


def _add_model_names(image, model_names, font_type=cv2.FONT_HERSHEY_PLAIN):
    org_image = image
    sizes = []
    for i, model_name in enumerate(model_names):
        text = f"{i + 1}: {model_name}"
        font_size, _ = _get_font_scale(org_image, text, height_coverage=0.05)
        sizes.append(font_size)
    min_size = min(sizes)
    for i, model_name in enumerate(model_names):
        text = f"{i + 1}: {model_name}"
        text_size = cv2.getTextSize(text, fontFace=font_type, fontScale=min_size, thickness=round(min_size))
        image = cv2.copyMakeBorder(image, 0, round(text_size[0][1] * 1.5), 0, 0,
                                   cv2.BORDER_CONSTANT, None, value=np.iinfo(image.dtype).max)
        cv2.putText(image, text, (0, image.shape[0]), font_type, min_size, 2, thickness=round(min_size))
    return image


def _get_shapes(images):
    shapes = set()
    for image in images:
        shapes.add(image.shape)
    return shapes


def _concat_vh(images):
    return cv2.vconcat([cv2.hconcat(list_h)
                        for list_h in images])


def hist_match(image, ref):
    if isinstance(image, torch.Tensor):
        image = image.detach().cpu().numpy().squeeze()
    if isinstance(ref, torch.Tensor):
        ref = ref.detach().cpu().numpy().squeeze()
    cropped_ref = ref[..., 3:-3, 3:-3]
    cropped_image = image[..., 3:-3, 3:-3]
    cropped_image = exposure.match_histograms(cropped_image, cropped_ref)
    image[..., 3:-3, 3:-3] = cropped_image
    return image


def hist_equal(image):
    if isinstance(image, torch.Tensor):
        image = image.detach().cpu().numpy().squeeze()
    cropped_image = image
    cropped_image = exposure.equalize_adapthist(cropped_image)
    image = cropped_image
    return image
