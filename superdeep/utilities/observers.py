"""
Observers - object that wait for particular event and somehow response to them.
Most of observers that can be found in this module are use to log information about traning process.

"""
import abc
import os
import csv
import time
import collections
import statistics
import torch
import math
import numpy as np
import cv2
from typing import Any, List, Union

import matplotlib.pyplot as plt
from torch.utils import data
from superdeep.training import events
import wandb

Path = Union[os.PathLike, str]
LogPoint = collections.namedtuple("LogPoint", ["name", "value", "epoch"])
AfterEpochDataToSave = collections.namedtuple("DataToSave", ["optimizer", "model",
                                                             "train_loss", "val_loss", "epoch"])
EpochValidationResults = collections.namedtuple("EpochValidationResults",
                                                ["epoch", "batch", "data"])

class Observer(abc.ABC):
    @abc.abstractmethod
    def on_event(self, event, data: Any):
        """
        Callback which is called every time observed object fires an event.

        :param event: Event to handle
        :param data: Data associated with event
        """


class Observable:
    """
    Objects that are observable can notify different observers of specific
    events.

    """

    def __init__(self):
        self.observers = []

    def add_observer(self, observer: Observer):
        """
        Adds observer so it can be notified of events.

        """
        self.observers.append(observer)

    def notify(self, event, data: Any):
        for observer in self.observers:
            observer.on_event(event, data)


class PyTorchModelSaver(Observer):
    def __init__(self, mode='V', ):
        self.mode = mode.lower()
        self.best_loss = None

    def on_event(self, event, data: AfterEpochDataToSave):
        if event == events.TraningEvents.EPOCH_FINISHED:
            if self.best_loss is None or self.best_loss > data.val_loss['total']:
                self.best_loss = data.val_loss['total']
                self.save_states(data)

    def save_states(self, data: AfterEpochDataToSave):
        out_path = os.path.join(wandb.run.dir, 'model.pth')
        torch.save({"model": data.model.state_dict(), "optimizer": data.optimizer.state_dict()}, out_path)
        try:
            wandb.save(out_path)
        except Exception as e:
            print(f"Failed to upload model to Weights&Biases, however it is still saved in {out_path}.")

class FirstValidationBatchSaver(Observer):
    def __init__(self):
        pass

    def on_event(self, event, data: EpochValidationResults):
        if event == events.EpochEvents.VALIDATION_BATCH_OUTPUT:
            if data.batch == 0:
                if isinstance(data.data, dict):
                    for key, value in data.data.items():
                        wandb.log({f"predictions/{key}": wandb.Image(value.cpu()[0, 0, 3:-3, 3:-3])},
                                  step=data.epoch)
                else:
                    wandb.log({"val_pred": wandb.Image(data.data.cpu()[0, 0, 3:-3, 3:-3])}, step=data.epoch)


class LossLogger(Observer):
    def __init__(self):
        pass

    def on_event(self, event, data: AfterEpochDataToSave):
        if event == events.TraningEvents.EPOCH_FINISHED:
            wandb.log({'validation_loss': data.val_loss, 'training_loss': data.train_loss,
                       "epoch": data.epoch})


class ProgressLogger(Observer):
    class ProgressBar:
        def __init__(self, total: int, prefix: str):
            self._total = total
            self._iter = 0
            self._prefix = prefix
            self._started = time.time()
            if total != 0:
                self.tick()

        def tick(self, loss=None):
            length = 100
            decimals = 1
            current = min(100.0, 100.0 * self._iter / float(self._total))
            percent = ("{0:." + str(decimals) + "f}").format(current)
            filledLength = int(current)
            bar = 'X' * filledLength + '_' * (length - filledLength)
            # print('\r%s %s |%s| %s%% %s' % (self.prefix, f'({self._iter}\\{self._total})', bar, percent, "Completed"),
            #       end='', flush=True)
            elapsed_time = time.time() - self._started
            if self._iter != 0:
                seconds_per_iteration = round(elapsed_time / self._iter, 2)
                time_left = seconds_per_iteration * (self._total - self._iter)
                time_left = time.strftime("%M:%S", time.gmtime(round(time_left, 2)))
            else:
                seconds_per_iteration = '-'
                time_left = '-'

            print(f'\r{self._prefix} ({self._iter}\\{self._total}) |{bar}| {percent}% '
                  f'[{time.strftime("%M:%S", time.gmtime(elapsed_time))}<{time_left}, {seconds_per_iteration}s/it] ' +
                  f'Loss: {loss}',
                  end='', flush=True)
            self._iter = self._iter + 1

        def finish(self):
            print()

    def __init__(self, name: str):
        self._name = name
        self._pb = None

    def on_event(self, event, data: Any):
        if event == events.EpochEvents.EPOCH_STARTED:
            self._pb = ProgressLogger.ProgressBar(math.ceil(float(data[1]) / float(data[2])),
                                                  f"{self._name}({str(data[0]).zfill(4)}):")
        elif event == events.EpochEvents.BATCH_FINISHED:
            self._pb.tick(data[-1])
        elif event in events.EpochEvents.EPOCH_FINISHED:
            self._pb.finish()
