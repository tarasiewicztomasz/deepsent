import torch
import os
from pathlib import Path
import typing
from superdeep.models import NearestNeighbors, LRs, Bicubic
from superdeep.data import collation
from superdeep.algorithms import EvoNet
from superdeep.utilities.configs import Config
import fnmatch


class ModelLoader:
    """
    A class responsible for loading and instantiating already trained models.
    """
    def __init__(self, log_dir, device='cuda:0', config_required=True, interp_scale=None, checkpoint_type: str = 'best'):
        assert checkpoint_type in ['last', 'best'], 'Choose a proper checkpoint type: last or best.'
        self.checkpoint_type = checkpoint_type
        self._create_predefined_models(interp_scale)
        self.log_dir = Path(log_dir)
        self.device = device
        self.config_required = config_required
        self.configs = self.__get_models()

    @property
    def available_models(self):
        return [config.model_name for config in self.configs]

    def _create_predefined_models(self, scale):
        self.predefined = [
            Config(model=Bicubic, model_name='BicubicMISR',
                   collate_function=collation.SREntryMultipleImageCollation,
                   model_params={'scale': scale}),
            Config(model=Bicubic, model_name='BicubicSISR', collate_function=collation.SREntrySingleImageCollation,
                   model_params={'scale': scale}),
            Config(model=NearestNeighbors, model_name='NearestNeighborsMISR',
                   collate_function=collation.SREntryMultipleImageCollation,
                   model_params={'scale': scale}),
            Config(model=NearestNeighbors, model_name='NearestNeighborsSISR',
                   collate_function=collation.SREntrySingleImageCollation,
                   model_params={'scale': scale}),
            Config(model=LRs, model_name='LRs',
                   collate_function=collation.SREntryMultipleImageCollation,
                   model_params={'scale': 1}, dataset_params={'normalize': False}),
            Config(model=LRs, model_name='norm_LRs',
                   collate_function=collation.SREntryMultipleImageCollation,
                   model_params={'scale': 1}, dataset_params={'normalize': 'lrs'}),
        ]

    def filter(self, substring: typing.Union[str, typing.List[str]] = ''):
        if substring is None:
            self.configs = []
        elif isinstance(substring, list):
            self.configs = list(filter(lambda x: x.model_name in substring, self.configs))
        else:
            self.configs = list(filter(lambda x: substring in x.model_name, self.configs))
        return self

    def __len__(self):
        return len(self.configs)

    def __getitem__(self, item):
        if isinstance(item, slice):
            def return_generator():
                items = (i for i in self.configs[item])
                for i in items:
                    yield self.__get_single_item(i)
            return return_generator()
        else:
            return self.__get_single_item(item)

    def __get_models(self):
        configs = [self._get_config(str(name)) for name in self.log_dir.glob('*')] + self.predefined
        return list(filter(lambda x: x is not None, configs))

    def _load_model(self, model, model_dir, epoch=None):
        model_dir = Path(model_dir)
        saved_states = (model_dir / 'saved_states')
        if saved_states.is_dir():
            weights_path = saved_states / f'{self.checkpoint_type}_weights'
        else:
            weights_path = model_dir / 'weights'
        assert weights_path.is_file(), f'Could not load the model. No such file: {weights_path}.'
        state_dict = torch.load(str(weights_path), map_location=self.device)
        if isinstance(state_dict, torch.nn.Module):
            return state_dict
        model.load_state_dict(state_dict)
        return model

    def __get_single_item(self, item):
        config = self.configs[item]
        if config.model in [Bicubic, NearestNeighbors, LRs, EvoNet]:
            model = config.model(**config.model_params)
            return model, config
        model = config.model(**config.model_params)
        logdir = os.path.join(self.log_dir, config.model_name)
        model = self._load_model(model, logdir, config.best_epoch)
        return model, config

    def _get_config(self, path) -> typing.Union[Config, None]:
        if os.path.isfile(os.path.join(path, Config.FILENAME)):
            config = Config.from_file(path)
        else:
            if self.config_required:
                return None
            config = Config.from_logdir(path)
        return config
