from superdeep.data.configurator import SubsetEnum


def str_to_subset(subset_str):
    if subset_str.upper() == "TRAIN":
        return SubsetEnum.TRAIN
    elif subset_str.upper() == "VALID":
        return SubsetEnum.VALIDATION
    elif subset_str.upper() == "TEST":
        return SubsetEnum.TEST
    elif subset_str.upper() == "ALL":
        return SubsetEnum.ALL
    else:
        raise ValueError(f"Subset {subset_str.upper()} not recognised")