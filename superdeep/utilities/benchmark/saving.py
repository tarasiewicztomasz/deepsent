import os
from pathlib import Path
import pandas as pd
import numpy as np
import torch
from PIL import Image
import wandb
from typing import Union


class ImageSaver:
    def __init__(self, out_dir: Union[Path, str], model_name: str, save_images: bool = True):
        self.out_dir = (out_dir if isinstance(out_dir, Path) else Path(out_dir)) / model_name
        self._save_func = self._save if save_images else lambda *args, **kwargs: None
        self.model_name = model_name

    def save(self, image, name: str, format: str = 'tiff'):
        self._save_func(image, name, format=format)

    def _save(self, image, name: str, band: str = '', format: str = 'tiff', model: str = None):
        if isinstance(image, dict):
            self._save_multiband(image, name, format=format)
        else:
            self._save_single_image(image, name, band=band, format=format)

    def _save_single_image(self, image, name: str, band: str = '', format: str = 'tiff'):
        if image is None:
            return
        out_path = self.out_dir / name / band
        if isinstance(image, torch.Tensor):
            image = image.cpu().numpy().squeeze()

        if image.ndim > 2 and image.shape[0] > 1:
            os.makedirs(out_path, exist_ok=True)
            for i in range(image.shape[0]):
                img = Image.fromarray(image[i, :, :])
                img.save((out_path / f'lr_{str(i).zfill(2)}').with_suffix(f'.{format}'))
        else:
            assert image.ndim == 2, f'Image must be 2D, got {image.ndim}D'
            image = Image.fromarray(image)
            os.makedirs(out_path.parent, exist_ok=True)
            image.save(out_path.with_suffix(f'.{format}'))

    def _save_multiband(self, image: dict, name: str, format: str = 'tiff'):
        for k, v in image.items():
            self._save_single_image(v, name, band=k, format=format)


class ImageLogger:
    def __init__(self, max_images: int = 0):
        self.scenes = {}
        self.max_images = max_images

    def add(self, image, scene_name):
        if scene_name in self.scenes or len(self.scenes) < self.max_images:
            self._add(image, scene_name)
        return

    def _add(self, image, scene_name):
        if scene_name not in self.scenes:
            self.scenes[scene_name] = {}
        if isinstance(image, dict):
            for key, value in image.items():
                if key not in self.scenes[scene_name]:
                    self.scenes[scene_name][key] = value
        else:
            self.scenes[scene_name] = image
        return

    def log(self):
        wandb_images_dict = {}
        wandb_images_list = []
        for name, images in self.scenes.items():
            if isinstance(images, dict):
                for band, image in images.items():
                    if band not in wandb_images_dict:
                        wandb_images_dict[band] = []
                    wandb_images_dict[band].append(wandb.Image(image, caption=f"{name}"))
            else:
                wandb_images_list.append(wandb.Image(images, caption=name))
        if len(wandb_images_dict) > 0:
            wandb.log({'benchmark_images': wandb_images_dict})
        if len(wandb_images_list) > 0:
            wandb.log({'benchmark_images': wandb_images_list})


class TableLogger:
    def __init__(self, table_name='benchmark_scores'):
        self.table_name = table_name
        self.table_headers = set()
        self.table_rows = []
        self.id = None

    def add_row(self, row):
        row_columns = row.get_columns()
        if self.id and self.id not in row_columns:
            raise ValueError(f"Row must contain id column {self.id}")

        elif self.id and self.id in row_columns and len(self._find_row_indices(row.get_value(self.id))) > 0:
            id_value = row.get_value(self.id)
            matching_rows_indices = self._find_row_indices(id_value)
            assert len(matching_rows_indices) <= 1, f"Multiple rows with id {id_value}"
            self.table_rows[matching_rows_indices[0]] = self.table_rows[matching_rows_indices[0]].merge(row)
        else:
            self.table_rows.append(row)
        self.table_headers.update(row_columns)

    def _find_row_indices(self, id_value):
        return [i for i, row in enumerate(self.table_rows) if row.get_value(self.id) == id_value]

    def _convert_rows_to_lists(self):
        rows = []
        columns = self.get_columns()
        row: TableRow
        for row in self.table_rows:
            new_row = [row.get_value(column) for column in columns]
            rows.append(new_row)
        self.show_columns()
        return columns, rows

    def log(self):
        columns, rows = self._convert_rows_to_lists()
        if len(rows) == 0:
            return
        save_path = (Path(wandb.run.dir) / "../../..").resolve() / 'benchmark_scores.xlsx'
        wandb_table = wandb.Table(columns=columns, data=rows)
        data_frame: pd.DataFrame = wandb_table.get_dataframe()
        data_frame.to_excel(save_path, index=False, engine='openpyxl')
        print(f"Score table saved to {save_path}")
        wandb.log({self.table_name: wandb_table})

    def get_columns(self):
        columns = list(self.table_headers)
        columns.sort()
        if self.id and self.id in columns:
            columns.remove(self.id)
            columns.insert(0, self.id)
        return columns

    def show_columns(self):
        print(self.get_columns())

    def set_id(self, id_column):
        self.id = id_column
        return self


class TableRow:
    def __init__(self):
        self.data = dict()

    def add_data(self, value, *keys):
        key = str.join('_', keys)
        self.data[key] = value
        return self

    def get_columns(self):
        return list(self.data.keys())

    def get_value(self, key):
        if key in self.data:
            return self.data[key]
        else:
            return None

    def merge(self, row):
        self.data.update(row.data)
        return self
