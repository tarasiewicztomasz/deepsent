from matplotlib import pyplot as plt

from superdeep.image import transformations


def visualize_entry(dataset, index, title):
    f = plt.figure(figsize=(12, 7))
    plt.axis("off")
    plt.title(title)

    f.add_subplot(1, 2, 1)
    plt.imshow(dataset[index].hr_image)
    plt.title("HR image")

    f.add_subplot(1, 2, 2)
    plt.imshow(dataset[index].lr_images[0])
    plt.title("LR image")

    plt.show(block=True)


def visualize_sr_lr(sr, lr, title, show_bicubic=True):
    f = plt.figure(figsize=(12, 7))
    plt.axis("off")
    plt.title(title)

    f.add_subplot(1, 3, 1)
    plt.imshow(lr, cmap="Greys_r", interpolation='none')
    plt.title("LR")

    resize = transformations.Resize((sr.shape[0], sr.shape[1]), transformations.Filter.BICUBIC)
    lr_bicubic = resize(lr)
    f.add_subplot(1, 3, 2)
    plt.imshow(lr_bicubic, cmap="Greys_r")
    plt.title("Bicubic resize")

    f.add_subplot(1, 3, 3)
    plt.imshow(sr, cmap="Greys_r")
    plt.title("SRR")

    plt.show(block=True)
