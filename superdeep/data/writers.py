"""
Writing datasets to disk memory.

"""
import abc
import os
import pathlib
import typing

from superdeep.data import datasets
from superdeep.image import io

Path = typing.Union[str, os.PathLike]


class SREntryDatasetWriter(abc.ABC):
    @abc.abstractmethod
    def write(self, dataset: datasets.SREntryDataset):
        pass


class BaseDirectoryWriter(SREntryDatasetWriter):
    """
    Example of outputted directory structure.
    ::
        /---dataset
        +---img1
        |   |   hr.bmp
        |   |
        |   /---lrs
        |           lr1.bmp
        |           lr2.bmp
        |           lr3.bmp
        |
        +---img2
        |   |   hr.bmp
        |   |
        |   /---lrs
        |           lr1.bmp
        |           lr2.bmp
        |           lr3.bmp
        |
        /---img3
            |   hr.bmp
            |
            /---lrs
                    lr1.bmp
                    lr2.bmp
                    lr3.bmp

    """

    def __init__(self, root_directory: Path, filename_extension: str):
        self._root_directory = root_directory
        self._filename_extension = filename_extension

    def write(self, dataset: datasets.SREntryDataset):
        for sr_entry_index, sr_entry in enumerate(dataset, start=1):
            sr_entry_dir = os.path.join(self._root_directory, str(sr_entry_index))
            lrs_dir = os.path.join(sr_entry_dir, "lrs")
            pathlib.Path(lrs_dir).mkdir(parents=True, exist_ok=True)
            io.save_image(sr_entry.hr_image,
                          os.path.join(sr_entry_dir, "hr" + self._filename_extension))
            for lr_index, lr in enumerate(sr_entry.lr_images, start=1):
                io.save_image(lr, os.path.join(lrs_dir, str(lr_index) + self._filename_extension))
