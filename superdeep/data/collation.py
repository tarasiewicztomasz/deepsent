"""
Collation operations that are passed to the collate_fn argument in torch DataLoader constructor.
Provide conversion of input structures to torch tensors passed to model while training or inferring.
"""
from typing import List, Union

import numpy as np
import torch
from torchvision import transforms
from superdeep.data import SREntry, MultiBandSREntry
from superdeep.data import datasets


def _stack(tensor_list):
    if tensor_list is None:
        return None
    else:
        return torch.stack(tensor_list)


def non_deterministic_seed_gen(*args, **kwargs):
    while True:
        yield np.random.randint(0, np.iinfo(np.uint16).max)


def deterministic_seed_gen(start: int = 0, *args, **kwargs):
    while True:
        start += 1
        yield start


class Collation:
    """
    Collation base-class
    """

    def __init__(self, output_bands=None, random_band=None, deterministic=False, **kwargs):
        self._kwargs = kwargs
        self.output_bands = output_bands
        self.random_band = random_band
        self.seed_generator = {True: deterministic_seed_gen,
                               False: non_deterministic_seed_gen}.get(deterministic)()

    def __call__(self, batch: List[Union[SREntry, MultiBandSREntry]]):
        seed = next(self.seed_generator)
        if isinstance(batch[0], SREntry):
            return self._collate(batch, seed=seed)
        elif isinstance(batch[0], MultiBandSREntry):
            batched_multiband_entry = {band: self._collate([entry[band] for entry in batch], seed=seed)
                                       for band in batch[0].keys()}
            if self.output_bands is None:
                output_bands = list(batched_multiband_entry.keys())
            else:
                output_bands = self.output_bands
            return MultiBandSREntry(batched_multiband_entry,
                                    output_bands=output_bands, is_collated=True)
        else:
            raise TypeError('Entry has to be of type "SREntry" or "MultiBandSREntry".')

    def _collate(self, batch: List[datasets.SREntry], **args):
        raise NotImplementedError


class SREntrySingleImageCollation(Collation):
    """
    Collates entries into batches of tensors. This takes only first
    lr image from the list of lr images that are in the example.

    """

    def __init__(self, image_index: int = 0, output_bands=None, random_band: bool = False, **kwargs):
        super().__init__(output_bands, random_band, **kwargs)
        self._image_index = image_index

    def _collate(self, batch: List[datasets.SREntry], **args):
        lr_batch = _stack([transforms.ToTensor()(entry.lr_images[self._image_index]) for entry in batch])
        hr_batch = _stack([transforms.ToTensor()(entry.hr_image)
                           for entry in batch]) if batch[0].hr_image is not None else None
        lr_masks = _stack([transforms.ToTensor()(entry.lr_masks[self._image_index])
                           for entry in batch]) if batch[0].lr_masks is not None else None
        hr_masks = _stack([transforms.ToTensor()(entry.hr_mask)
                           for entry in batch]) if batch[0].hr_mask is not None else None
        names = [entry.name for entry in batch] if batch[0].name is not None else None
        bands = set()
        for entry in batch:
            bands.add(entry.band)
        assert len(bands) == 1, 'All entries in the batch have to have the same band.'
        band = bands.pop()
        return SREntry(lr_images=lr_batch, hr_image=hr_batch, lr_masks=lr_masks,
                       hr_mask=hr_masks, name=names, is_collated=True, band=band)


class SREntrySingleImageCollationRepeated(Collation):
    """
    Collates entries into batches of tensors. This takes only one
    lr image from the list of lr images that are in the example and repeats it n times.

    """

    def __init__(self, image_index: int = 0, repeats=9, output_bands=None, random_band: bool = False, **kwargs):
        super().__init__(output_bands, random_band, **kwargs)
        self._image_index = image_index
        self.repeats = repeats

    def _collate(self, batch: List[datasets.SREntry], **args):
        lr_batch = _stack([transforms.ToTensor()(entry.lr_images[self._image_index]) for entry in batch])
        lr_batch = torch.repeat_interleave(lr_batch, self.repeats, 1)
        hr_batch = _stack([transforms.ToTensor()(entry.hr_image)
                           for entry in batch]) if batch[0].hr_image is not None else None
        lr_masks = _stack([transforms.ToTensor()(entry.lr_masks[self._image_index])
                           for entry in batch]) if batch[0].lr_masks is not None else None
        if lr_masks is not None:
            lr_masks = torch.repeat_interleave(lr_masks, self.repeats, 1)
        hr_masks = _stack([transforms.ToTensor()(entry.hr_mask)
                           for entry in batch]) if batch[0].hr_mask is not None else None
        names = [entry.name for entry in batch] if batch[0].name is not None else None
        bands = set()
        for entry in batch:
            bands.add(entry.band)
        assert len(bands) == 1, 'All entries in the batch have to have the same band.'
        band = bands.pop()
        return SREntry(lr_images=lr_batch, hr_image=hr_batch, lr_masks=lr_masks,
                       hr_mask=hr_masks, name=names, is_collated=True, band=band)


class SREntryMultipleImageCollation(Collation):
    """
    Collates entries into batches of tensors, stacking lr images to form a higher-dimensionality tensor.
    """

    def __init__(self, permute=False, output_bands=None, random_band: bool = False, **kwargs):
        super().__init__(output_bands, random_band, **kwargs)
        self.permute = permute

    def _collate(self, batch, seed: int = None):
        if self.permute:
            permutations = [np.random.RandomState(seed).permutation(len(entry.lr_images)) for entry in batch]
        else:
            permutations = [np.arange(len(batch[0].lr_images))] * len(batch)
        lr_batch = [(np.stack(entry.lr_images, 2))[:, :, perm] for entry, perm in zip(batch, permutations)]
        try:
            lr_batch = _stack([transforms.ToTensor()(lrs) for lrs in lr_batch])
        except Exception as e:
            for b in batch:
                if len(b.lr_images) > 9:
                    print(b.name)
            raise e
        hr_batch = _stack([transforms.ToTensor()(entry.hr_image)
                           for entry in batch]) if batch[0].hr_image is not None else None

        lr_masks = _stack([transforms.ToTensor()(np.stack(entry.lr_masks, 2))[:, :, perm]
                           for entry, perm in zip(batch, permutations)]) if batch[0].lr_masks is not None else None
        hr_masks = _stack([transforms.ToTensor()(entry.hr_mask)
                           for entry in batch]) if batch[0].hr_mask is not None else None
        names = [entry.name for entry in batch] if batch[0].name is not None else None
        bands = set()
        for entry in batch:
            bands.add(entry.band)
        assert len(bands) == 1, 'All entries in the batch have to have the same band.'
        band = bands.pop()
        return SREntry(lr_images=lr_batch, hr_image=hr_batch, lr_masks=lr_masks,
                       hr_mask=hr_masks, name=names, is_collated=True, band=band)


class SREntryDeepSumCollation(Collation):
    """
    Collation to be used in DeepSum trainings:
        N C D H W
        hr: N 1 1 94 94
        lrs: N 1 9 94 94
    """

    @classmethod
    def lrs_for_deepsum(cls, lr_images):
        assert len(lr_images) >= 9, "At least 9 images required for DeepSum to work."
        return (torch.stack([torch.from_numpy(np.float32(lr)) for lr in lr_images][:9])
                .reshape((1, 9, lr_images[0].shape[0], lr_images[0].shape[1])))

    def __call__(self, batch):
        lrs_all = []
        hrs_all = []
        for srentry in batch:
            hr = srentry.hr_image
            lrs_all.append(SREntryDeepSumCollation.lrs_for_deepsum(srentry.lr_images))
            hrs_all.append(torch.from_numpy(np.float32(hr)).reshape((1, 1, hr.shape[0], hr.shape[1])))

        lrs_result = torch.stack(lrs_all)
        hrs_result = torch.stack(hrs_all)

        return lrs_result, hrs_result
