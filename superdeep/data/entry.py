"""
Data package consists of data structures and routines mostly connected to to building super-resolution
datasets and performing some (pre)processing on them.

"""
import typing
from typing import Union
import numpy as np
import enum

import pandas as pd
from torch import Tensor
from itertools import chain

import os


class EntryElements(enum.IntEnum):
    LR = 0
    HR = 1
    LR_MASKS = 2
    HR_MASK = 3


class SREntry:
    """
    SREntry (SR as in Super-Resolution) represents pair of high resolution image and corresponding to
        it low resolution images.
    """
    __slots__ = ["hr_image", "lr_images", "hr_mask", 'lr_masks', 'name', 'is_collated', 'band']

    def __init__(self, hr_image: Union[np.ndarray, Tensor, None],
                 lr_images: Union[typing.Sequence[np.ndarray], Tensor, None],
                 hr_mask: Union[np.ndarray, Tensor, None] = None,
                 lr_masks: Union[typing.Sequence[np.ndarray], Tensor, None] = None,
                 name: typing.Union[str, typing.List[str]] = None,
                 is_collated: bool = False,
                 band=None):
        self.hr_image = hr_image
        self.lr_images = lr_images
        self.hr_mask = hr_mask
        self.lr_masks = lr_masks if lr_masks is None else lr_masks
        self.name = name
        self.is_collated = is_collated
        self.band = 'None' if band is None else band

    def __eq__(self, other):
        hr_equality = np.array_equal(self.hr_image, other.hr_image)
        lrs_equality = all(np.array_equal(lhs, rhs) for lhs, rhs in
                           zip(self.lr_images, other.lr_images))
        return hr_equality and lrs_equality

    def to(self, device):
        assert self.is_collated, f"Can't send entry to device({device}) if it is not collated."
        self.lr_images = self.lr_images.to(device)
        if self.hr_image is not None:
            self.hr_image = self.hr_image.to(device)
        if self.lr_masks is not None:
            self.lr_masks = self.lr_masks.to(device)
        if self.hr_mask is not None:
            self.hr_mask = self.hr_mask.to(device)
        return self


class MultiBandSREntry:
    def __init__(self, scene: typing.Dict[str, typing.Union[SREntry]] = None, input_bands=None,
                 output_bands=None, is_collated: bool = False):
        assert isinstance(scene, dict), "Multiband scene has to be of type dict!"
        self.is_collated = is_collated
        self.scene = scene if scene is not None else dict()
        self.input_bands = input_bands if input_bands is not None else list(scene.keys())
        self.output_bands = output_bands
        if not isinstance(self.input_bands, list):
            self.input_bands = [self.input_bands]
        if not isinstance(self.output_bands, list):
            self.output_bands = [self.output_bands]

    def __getitem__(self, item):
        assert isinstance(item, str), f"Wrong key type: expected 'str', got {type(item)}."
        return self.scene[item]

    def keys(self):
        return self.scene.keys()

    def values(self):
        return self.scene.values()

    def items(self):
        return self.scene.items()

    @property
    def bands(self):
        return list(self.scene.keys())

    @property
    def lr_images(self):
        return {k: self.scene[k].lr_images for k in self.input_bands}

    @property
    def hr_image(self):
        return {k: self.scene[k].hr_image for k in self.output_bands}

    @property
    def lr_masks(self):
        return {k: self.scene[k].lr_masks for k in self.input_bands}

    @property
    def hr_mask(self):
        return {k: self.scene[k].hr_mask for k in self.output_bands}

    @property
    def name(self):
        return list(set(chain.from_iterable([x.name for x in self.scene.values()])))

    def to(self, device):
        assert self.is_collated, f"Can't send entry to device({device}) if it is not collated."
        for k in self.scene.keys():
            self.scene[k] = self.scene[k].to(device)
        return self


