"""
Module with transformations over super-resolution entries aka :class:`SREntry`.
Super-resolution entry is a high resolution image with its corresponding one or more low resolution images.


"""
import abc
import enum
import numpy as np
import os
from typing import Callable, Dict, Iterator, NamedTuple, Tuple, Union, Iterable, List, Sequence

import torchdatasets as td
from superdeep.data import SREntry, MultiBandSREntry, EntryElements
from superdeep.data import datasets

from superdeep.image import utilities

DEFAULT_REGISTRATION_PARAMETERS = {
    "registration.algorithm": "ecc",
    "registration.ecc.numberOfIterations": "50",
    "registration.ecc.stopEpsilon": "1.0E-12",
    "registration.ecc.warpMode": "0"
}

ImageTransformation = Callable[[np.ndarray], np.ndarray]


class SREntryTransformation(abc.ABC):
    """
    Base interface for defining all transformations of SREntry objects.
    """

    @abc.abstractmethod
    def __call__(self, entry: SREntry) -> SREntry:
        pass


class SREntryTransformationBuilder:
    class SREntryTransformationBuildError(Exception):
        class Reason(enum.Enum):
            HR_TRANFORMATION_NOT_SET = "Transformation for HR image was not set."
            LR_TRANSFORMATION_NOT_SET = "Transformation for LR images was not set."
            TRANSFORMATIONS_NOT_SET = "Transformations were not set."

        def __init__(self, reason: Reason):
            self._reason = reason

        def __str__(self):
            return self._reason.value

    """
    Fluent builder for defining transformations of SREntry based datasets.
    Allows for defining transformations of LR and HR images separately.
    While describing transformations both HR and LR should be declared, i.e. if not transforming HR
    "without_hr_transformation()" must be explicitely called.
    """

    def __init__(self):
        self._transformations = dict.fromkeys(list(EntryElements))

    def with_transformation(self, transformation: ImageTransformation,
                            target: Union[EntryElements, Iterable[EntryElements]] = None) \
            -> "SREntryTransformationBuilder":
        if target is None:
            target = list(EntryElements)
        if not isinstance(target, Iterable):
            target = [target]
        for key in target:
            self._transformations[key] = transformation
        return self

    def without_transformation(self,
                               target: Union[EntryElements, Iterable[EntryElements]]
                               ) -> "SREntryTransformationBuilder":
        if not isinstance(target, Iterable):
            target = [target]
        for key in target:
            self._transformations[key] = lambda x: x
        return self

    def without_rest_transformation(self) -> "SREntryTransformationBuilder":
        for key in self._transformations.keys():
            if self._transformations[key] is None:
                self._transformations[key] = lambda x: x
        return self

    def build(self) -> Callable:
        if not any(list(self._transformations.values())):
            raise SREntryTransformationBuilder.SREntryTransformationBuildError(
                SREntryTransformationBuilder.SREntryTransformationBuildError.Reason.TRANSFORMATIONS_NOT_SET)

        def transformation(entry: SREntry) -> SREntry:
            new_lrs = [self._transformations[EntryElements.LR](lr) for lr in entry.lr_images]
            new_hr = self._transformations[EntryElements.HR](entry.hr_image) if entry.hr_image is not None else None
            new_lr_masks = [self._transformations[EntryElements.LR_MASKS](lr_mask) for lr_mask in entry.lr_masks] \
                if entry.lr_masks is not None else None
            new_hr_mask = self._transformations[EntryElements.HR_MASK](entry.hr_mask) \
                if entry.hr_mask is not None else None
            return SREntry(new_hr, new_lrs, new_hr_mask, new_lr_masks, name=entry.name, band=entry.band)

        return transformation


def mask_lrs(entry: SREntry):
    assert entry.lr_masks is not None and len(entry.lr_images) == len(entry.lr_masks),\
        f"Can't match lists of different length. LRS: {len(entry.lr_images)}, LR_MASKS: " \
        f"{ len(entry.lr_masks) if entry.lr_masks is not None else None}"

    for i, lr in enumerate(entry.lr_images):
        entry.lr_images[i] = (lr * entry.lr_masks[i]).astype(lr.dtype)
    return entry

def register_entry(entry: SREntry, ref_index: int = None):
    from skimage.registration import phase_cross_correlation
    from scipy.ndimage import shift
    if ref_index is None:
        ref_index = len(entry.lr_images) - 1
    lrs = entry.lr_images
    lr_masks = entry.lr_masks if entry.lr_masks is not None else [None] * len(lrs)
    for i in range(len(lrs)):
        shifts = phase_cross_correlation(lrs[ref_index], lrs[i], reference_mask=lr_masks[ref_index],
                                         moving_mask=lr_masks[i], return_error=False, upsample_factor=100,)
        # shifts = phase_cross_correlation(lrs[ref_index], lrs[i], return_error=False, upsample_factor=100)
        entry.lr_images[i] = shift(lrs[i], shifts, mode='reflect')
        if lr_masks[i] is not None:
            entry.lr_masks[i] = shift(lr_masks[i], shifts, mode='constant', cval=0)
    return entry


class PatchConfig(NamedTuple):
    shape: Tuple[int, ...]
    stride: Tuple[int, ...]


class PatchSREntryDataset(td.Dataset):
    def __init__(self, hr_config: PatchConfig, lr_config: PatchConfig = None, same_shapes: bool = True):
        super().__init__()
        self.hr_config = hr_config
        self.lr_config = lr_config
        self.varying_resolutions = False
        self._patches_per_image = None
        self.dataset = None
        self.same_shapes = same_shapes

    def __call__(self, dataset: datasets.SREntryDataset):  # -> Iterator[Union[SREntry, MultiBandSREntry]]:
        from tqdm import tqdm
        self.dataset = dataset
        if self._patches_per_image is None:
            if self.same_shapes:
                patches = self.get_patches(dataset[0], index_only=True)
                self._patches_per_image = [len(patches)]*len(dataset)
                return self
            self._patches_per_image = [len(self.get_patches(entry, index_only=True)) for entry in tqdm(dataset)]
        return self
        # for entry in dataset:
        #     patches = self.get_patches(entry)
        #     for patch in patches:
        #         yield patch

    def __len__(self):
        return sum(self._patches_per_image)

    def __getitem__(self, item):
        cumsum = 0
        entry_index = None
        patch_index = None
        for i, x in enumerate(self._patches_per_image):
            if cumsum + x > item:
                entry_index = i
                patch_index = item-cumsum
                break
            cumsum += x
        entry = self.dataset[entry_index]
        return self.get_patches(entry)[patch_index]

    def get_patches(self, entry: Union[SREntry, MultiBandSREntry], index_only: bool = False)\
            -> Sequence[Union[SREntry, MultiBandSREntry, int]]:
        if isinstance(entry, SREntry):
            return [x for x in self._crop(entry)]
        elif isinstance(entry, MultiBandSREntry):
            max_lr_shape = max(set([x.lr_images[0].shape for x in entry.values()]))
            max_hr_shape = max(set([x.hr_image.shape for x in entry.values()]))
            patches = {band: self._crop(sr_entry, max_lr_shape=max_lr_shape,
                                        max_hr_shape=max_hr_shape, index_only=index_only)
                       for band, sr_entry in entry.items()}
            if index_only:
                return list(zip(*patches.values()))
            assert len(set({len(x) for x in patches.values()})) == 1, "Different number of patches for multiband entry!"
            return [MultiBandSREntry(dict(zip(patches, i))) for i in zip(*patches.values())]
        else:
            raise TypeError('Entry has to be of type "SREntry" or "MultiBandSREntry".')

    def _crop(self, entry: SREntry, max_lr_shape=None, max_hr_shape=None, index_only: bool = False)\
            -> List[Union[SREntry, int]]:
        hr_scale, lr_scale = 1., 1.
        if max_lr_shape is not None and max_hr_shape is not None:
            lr_scale = max_lr_shape[0] / entry.lr_images[0].shape[0]
            hr_scale = max_hr_shape[0] / entry.hr_image.shape[0]
        lr_config = self.lr_config
        hr_config = self.hr_config
        if lr_config is None:
            hr_shape = entry.hr_image.shape
            lr_shape = entry.lr_images[0].shape
            shape_ratio = tuple(map(lambda x, y: x / y, hr_shape, lr_shape))

            lr_config = PatchConfig(
                tuple(map(lambda x, y: round(x / y), self.hr_config.shape, shape_ratio)),
                tuple(map(lambda x, y: round(x / y), self.hr_config.stride, shape_ratio))
            )
        # !!!!! Comment line below if maginfication is the same across all bands !!!!!
        hr_scale, lr_scale = 1.0, 1.0


        hr_shape = round(hr_config.shape[0] / hr_scale), round(hr_config.shape[1] / hr_scale)
        hr_stride = round(hr_config.stride[0] / hr_scale), round(hr_config.stride[1] / hr_scale)
        lr_shape = round(lr_config.shape[0] / lr_scale), round(lr_config.shape[1] / lr_scale)
        lr_stride = round(lr_config.stride[0] / lr_scale), round(lr_config.stride[1] / lr_scale)

        assert hr_shape[0] * hr_scale == hr_config.shape[0] and hr_shape[1] * hr_scale == hr_config.shape[1] and \
               hr_stride[0] * hr_scale == hr_config.stride[0] and hr_stride[1] * hr_scale == hr_config.stride[1] \
               and lr_shape[0] * lr_scale == lr_config.shape[0] and lr_shape[1] * lr_scale == lr_config.shape[1] and \
               lr_stride[0] * lr_scale == lr_config.stride[0] and lr_stride[1] * lr_scale == lr_config.stride[1], \
            "Shape mismatch when patching multiple bands! Please assure that config is divisible by all coefficients: "\
            f"HR shape: {hr_shape}, LR shape: {lr_shape}"
        if index_only:
            import itertools
            starting_rows_range = range(0, entry.hr_image.shape[0], hr_stride[0])
            starting_columns_range = range(0, entry.hr_image.shape[1], hr_stride[1])
            starting_row_indices = itertools.takewhile(
                lambda index: index + hr_shape[0] <= entry.hr_image.shape[0],
                starting_rows_range
            )
            starting_column_indices = itertools.takewhile(
                lambda index: index + hr_shape[1] <= entry.hr_image.shape[1],
                starting_columns_range
            )
            return list(itertools.product(starting_row_indices, starting_column_indices))
        hr_patches = list(utilities.slide_window_over_image(entry.hr_image, hr_shape, hr_stride))
        lr_patch_groups = [
            list(utilities.slide_window_over_image(lr_image, lr_shape, lr_stride)) for
            lr_image in
            entry.lr_images]

        hr_mask_patches = None
        lr_masks_patch_groups = None
        if entry.hr_mask is not None:
            hr_mask_patches = list(utilities.slide_window_over_image(entry.hr_mask, hr_shape,
                                                                     hr_stride))
        if entry.lr_masks is not None:
            lr_masks_patch_groups = [
                list(utilities.slide_window_over_image(lr_image, lr_shape, lr_stride)) for
                lr_image in entry.lr_masks]
        return [datasets.SREntry(hr_patch,
                                 [lr_patches[i] for lr_patches in lr_patch_groups],
                                 hr_mask_patches[i] if hr_mask_patches is not None else None,
                                 [lr_masks_patches[i] for lr_masks_patches in lr_masks_patch_groups]
                                 if lr_masks_patch_groups is not None else None,
                                 name=f"{entry.name}_{str(i).zfill(2)}",
                                 band=entry.band)
                for i, hr_patch in enumerate(hr_patches)]
