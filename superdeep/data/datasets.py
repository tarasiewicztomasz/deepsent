"""Implementations of torch.data.Dataset class."""
import os
from pathlib import Path
import copy
import numbers
import warnings
import numpy as np
import enum
from typing import Any, Callable, Sequence, Iterable, Union, List
from torch.utils import data
from superdeep.data import SREntry, MultiBandSREntry
from superdeep.image import io
import torchdatasets as td

from superdeep.utilities.filesystem import DirectoryContents

SREntryTransformation = Callable[[SREntry], SREntry]


class Order(enum.Enum):
    ASC = True
    DESC = False


class EmptyDataset(data.Dataset):
    def __len__(self):
        return 0

    def __getitem__(self, item):
        raise RuntimeError("getitem called on empty dataset.")


class ImageDatasetFromDirectory(data.Dataset):
    """
    Represents a set of images stored in a flat directory.

    """

    def __init__(self, directory: str):
        self.directory = directory
        self._image_entries = DirectoryContents.list_images_only(directory)
        self._images_count = len(self._image_entries)

    def __len__(self):
        return self._images_count

    def __getitem__(self, item):
        return io.read_image(self._image_entries[item].path)


class TransformedDataset(data.Dataset):
    """
    Represents a new dataset with transformations applied on it.
    Creates a new dataset, applying transformations on each item of the original dataset.

    """

    def __init__(self, dataset: data.Dataset, transform: Callable[[Any], Any]):
        """
        :param dataset: Dataset to perform transformation on.
        :param transform: Callable that receives input and produces output of
            the same type.
        """
        self._dataset = dataset
        self._transform = transform

    def __len__(self):
        return len(self._dataset)

    def __getitem__(self, item):
        return self._transform(self._dataset[item])


class DatasetCombiner(data.Dataset):
    """
    Combines (zips) two datasets.
    The new dataset has items consisting in tuples of images from two original datasets.
    Input datasets must be of the same length.
    Returned items are in a form of tuples.

    e.g. having original dataset [a,b,c] and [A,B,C] the resulting dataset will be [(a, A),(b, B),(c, C)]

    """

    def __init__(self, first_dataset, second_dataset):
        if len(first_dataset) != len(second_dataset):
            raise RuntimeError("Both datasets must consist of the same amount of items.")
        self.first_dataset = first_dataset
        self.second_dataset = second_dataset
        self.items_count = len(first_dataset)

    def __len__(self):
        return len(self.first_dataset)

    def __getitem__(self, item):
        return self.first_dataset[item], self.second_dataset[item]


class SREntryDataset(td.Dataset):
    """
    data.Dataset with type hints for :class:`SREntry`.
    Allows for operations like:
     - concatenating datasets (__add__)
     - picking a subset of random elements of the dataset (pick_randomly)
     - taking sublist of elements of the dataset (take)
     - reversing, shuffling, sorting and filtering the dataset

    """

    def __getitem__(self, index) -> SREntry:
        raise NotImplementedError

    def __len__(self):
        raise NotImplementedError

    def __add__(self, rhs: "SREntryDataset") -> "SREntryDataset":
        return ConcatenatedSREntryDataset(self, rhs)

    def transform(self, *transformations: SREntryTransformation) -> "SREntryDataset":
        raise NotImplementedError

    def get_raw_examples(self):
        raise NotImplementedError

    def pick_randomly(self, amount: int) -> "SREntryDataset":
        max_amount = len(self)
        if amount > max_amount:
            warnings.warn(f"Amount of samples to pick ({amount}) is bigger than dataset "
                          f"itself. Using all elements of dataset ({max_amount}).")
            amount = max_amount
        indices = np.random.choice(len(self), amount, replace=False)
        return SubsetSREntryDataset(self, indices)

    def take(self, amount: int, offset: int = 0) -> "SREntryDataset":
        max_amount = len(self)
        if amount + offset > max_amount:
            warnings.warn(f"Amount of samples to take ({amount}) is bigger than dataset "
                          f"itself. Using all elements of dataset ({max_amount}).")
            amount = max_amount
        return SubsetSREntryDataset(self, list(range(offset, amount + offset)))

    def shuffle(self, seed: int = None) -> "SREntryDataset":
        indices = list(range(len(self)))
        np.random.seed(seed)
        np.random.shuffle(indices)
        np.random.seed(None)
        return SubsetSREntryDataset(self, indices)

    def reverse(self) -> "SREntryDataset":
        indices = list(reversed(range(len(self))))
        return SubsetSREntryDataset(self, indices)

    def filter(self, predicate: Callable[[SREntry], bool]) -> "SubsetSREntryDataset":
        indices = []
        for entry_index, el in enumerate(self):
            if predicate(el):
                indices.append(entry_index)
        return SubsetSREntryDataset(self, indices)

    def sorted(self, key: Callable[[SREntry], numbers.Real], order: Order = Order.DESC) -> "SubsetSREntryDataset":
        values = [key(entry) for entry in self]
        best_indices = np.argsort(values)
        if order is Order.DESC:
            best_indices = np.flip(best_indices)
        return SubsetSREntryDataset(self, list(best_indices))


class SubsetSREntryDataset(SREntryDataset):
    def __init__(self, dataset: SREntryDataset, indices: Iterable[numbers.Integral]):
        super().__init__()
        self._dataset = dataset
        self._indices = list(indices)
        if len(self._indices) > len(self._dataset):
            raise ValueError(f"Too many indices. Subset cannot be greater than original dataset.")
        self._transformations = []

    def __len__(self):
        return len(self._indices)

    def get_raw_examples(self):
        return [self._dataset.get_raw_examples()[i] for i in self._indices]

    def __getitem__(self, item) -> SREntry:
        index = self._indices[item]
        entry = self._dataset[index]
        for transformation in self._transformations:
            entry = transformation(entry)
        return entry

    def transform(self, *transformations: SREntryTransformation) -> "SREntryDataset":
        copied = copy.deepcopy(self)
        copied._transformations += transformations
        return copied


class ConcatenatedSREntryDataset(SREntryDataset):
    """
    Class performing SREntryDataset concatenation, i.e. adding elements of dataset in a sequential way.
    Transformation are evaluated on the fly similarly to the LazySREntryDataset class.

    """

    def __init__(self, *datasets):
        super().__init__()
        self._concatenated = data.ConcatDataset(datasets)
        self._transformations = []

    def __len__(self):
        return len(self._concatenated)

    def __getitem__(self, item) -> SREntry:
        entry = self._concatenated[item]
        for transformation in self._transformations:
            entry = transformation(entry)
        return entry

    def transform(self, *transformations: SREntryTransformation) -> "ConcatenatedSREntryDataset":
        copied = copy.deepcopy(self)
        copied._transformations += transformations
        return copied

    def get_raw_examples(self):
        return list(self._concatenated)


class StaticSREntryDataset(SREntryDataset):
    """
    This dataset is a subscriptable sequence of examples.
    Static means that the whole dataset is read to the dynamic memory at once.

    """

    def __init__(self, examples: Sequence[SREntry]):
        super().__init__()
        self._examples = list(examples)

    def __len__(self):
        return len(self._examples)

    def __getitem__(self, item) -> SREntry:
        return self._examples[item]

    def get_raw_examples(self):
        return self._examples

    def transform(self, *transformations: SREntryTransformation) -> "StaticSREntryDataset":
        transformed_examples = self._examples
        for transformation in transformations:
            transformed_examples = map(transformation, transformed_examples)
        return StaticSREntryDataset(transformed_examples)

    @classmethod
    def from_directory(cls, directory_path: str, **kwargs):
        return cls(list(LazySREntryDataset(directory_path, **kwargs)))

    @classmethod
    def from_lazy_dataset(cls, dataset: "LazySREntryDataset") -> "StaticSREntryDataset":
        return cls(list(dataset))

    @classmethod
    def from_numpy_file(cls, lr_file: str, hr_file: str, lr_masks_file: str = None,
                        hr_masks_file: str = None, directory: str = None, is_NCHW: bool = True):
        def load_data(filename):
            if filename is None:
                return None
            if directory is not None:
                filename = os.path.join(directory, filename)
            data = list(np.load(filename).astype(np.float32))
            if not is_NCHW:
                data = [np.moveaxis(x, -1, 0) for x in data]
            data = [x.squeeze() for x in data]
            data = [list(x) if x.ndim == 3 else x for x in data]
            return data

        def check_masks(file, expected_length):
            mask_data = load_data(file)
            if mask_data is None:
                mask_data = [None for _ in range(expected_length)]
            assert len(mask_data) == expected_length, \
                f"Different number of examples for LR/HR images ({expected_length})" \
                f"and masks ({len(mask_data)}."
            return mask_data

        lr_data = load_data(lr_file)
        hr_data = load_data(hr_file)
        files_count = len(lr_data)
        assert files_count == len(hr_data), f'Different number of examples for LR images ({files_count}) and' \
                                            f'HR images ({len(hr_data)}).'
        lr_mask_data = check_masks(lr_masks_file, files_count)
        hr_mask_data = check_masks(hr_masks_file, files_count)
        dataset = [SREntry(hr, lrs, hr_mask, lr_masks)
                   for hr, lrs, hr_mask, lr_masks
                   in zip(hr_data, lr_data, hr_mask_data, lr_mask_data)]
        return cls(dataset)


class LazySREntryDataset(SREntryDataset):
    """
    Reads examples from directory on the fly.
    Example of directory structure that must be preserved if deepness is not set.
    Setting allowed_deepness parameter allows to have more subdirectories between
    the root directory and actual examples. Directories marked with '===' are optional.
    ::
        |dataset
        +---img1
        |   |   hr.bmp
        |   |
        |   |---lrs
        |           lr1.bmp
        |           lr2.bmp
        |           lr3.bmp
        |
        +---img2
        |   |   hr.bmp
        |   |
        |   |---lrs
        |           lr1.bmp
        |           lr2.bmp
        |           lr3.bmp
        |
        |---img3
            |   hr.bmp
            |
            |---lrs
            |       lr1.bmp
            |       lr2.bmp
            |       lr3.bmp
            |
            |
            |===masks
                    |   hr.bmp
                    |
                    |===lrs
                        |   lr1.bmp
                        |   lr2.bmp
                        |   lr3.bmp

    """

    def __init__(self, directory: str, additional_deepness: int = 0, bands: Union[str, List[str]] = None,
                 allowed_dirs: list = None, hr_name: str = '', lr_images=None):
        super().__init__()
        self._directory = directory
        self.lr_images = lr_images
        self.bands = bands
        # if isinstance(self.bands, list) and len(self.bands) == 1:
        #     self.bands = self.bands[0]
        self.hr_name = hr_name
        self._name_indices = self.__get_entry_name_indices(additional_deepness)
        self.allowed_dirs = allowed_dirs if allowed_dirs is not None else []
        self._examples_dirs = [sb.path for sb in DirectoryContents.deep_list_subdirs(directory,
                                                                                     deepness=additional_deepness)]
        self._transformations = []

    def __get_entry_name_indices(self, additional_deepness):
        name_start_index = -1
        name_end_index = 0
        if self.bands is not None:
            name_start_index -= 1
            name_end_index -= 1
        name_start_index -= additional_deepness
        if name_end_index == 0:
            name_end_index = None
        return name_start_index, name_end_index

    def __len__(self):
        return len(self._examples_dirs)

    def get_raw_examples(self):
        return self._examples_dirs

    def __getitem__(self, item) -> Union[SREntry, MultiBandSREntry]:
        example_path = self._examples_dirs[item]
        if isinstance(self.bands, str):
            return self._read_data(example_path, self.bands)
        elif isinstance(self.bands, list):
            return MultiBandSREntry({band: self._read_data(example_path, band) for band in self.bands})
        return self._read_data(example_path)

    def _read_data(self, example_path, band=None) -> SREntry:
        from pathlib import Path
        hr_image = None
        example_path = Path(example_path)
        if band is not None:
            example_path = example_path / band
        lr_folders = [x.name for x in example_path.glob('lr*') if x.is_dir()]
        assert len(lr_folders) == 1, f"Too many or no possible LR directories in {example_path}: {lr_folders}"
        lr_subdir_entries = DirectoryContents.list_subdirs(str(example_path), allowed_dirs=[lr_folders[0]])
        if self.hr_name is not None:
            hrs_entries = DirectoryContents.list_images_only(str(example_path))
            hrs_entries = list(filter(lambda x: self.hr_name in str(x), hrs_entries))
            # assert (len(lr_subdir_entries) == 1 and len(hrs_entries) == 1), \
            #     f"Scene directory ({example_path}) should contain: 1 HR image and 1 directory with LR images."
            if len(hrs_entries) > 0:
                hr_image = io.read_image(hrs_entries[0].path)

        lr_images = [io.read_image(lr_entry.path) for lr_entry in
                     (DirectoryContents.list_images_only(lr_subdir_entries[0].path))]
        if self.lr_images is not None:
            lr_images = lr_images[:self.lr_images]
        assert (len(lr_images) > 0), f"At least one LR image must be present in ({example_path})."

        masks_path = example_path / 'masks'
        hr_mask, lr_masks = None, None
        if os.path.isdir(masks_path):
            hr_mask_entry = DirectoryContents.list_images_only(str(masks_path))
            if len(hr_mask_entry) > 0:
                hr_mask = io.read_image(hr_mask_entry[0].path)
            lr_masks_subdir = DirectoryContents.list_subdirs(str(masks_path), allowed_dirs=[lr_folders[0]])
            if len(lr_masks_subdir) > 0:
                lr_masks = [io.read_image(lr_mask_entry.path) for lr_mask_entry in
                            (DirectoryContents.list_images_only(lr_masks_subdir[0].path))]
                if self.lr_images:
                    lr_masks = lr_masks[:self.lr_images]

        entry_name = '_'.join(list(example_path.parts)[self._name_indices[0]:self._name_indices[1]])
        sr_entry = SREntry(hr_image=hr_image, lr_images=lr_images, hr_mask=hr_mask, lr_masks=lr_masks,
                           name=entry_name, band=band)
        for transformation in self._transformations:
            sr_entry = transformation(sr_entry)
        return sr_entry

    def transform(self, *transformations: SREntryTransformation) -> "LazySREntryDataset":
        copied = copy.copy(self)
        copied._transformations = copy.copy(self._transformations)
        copied._transformations += transformations
        return copied


class DistributedLazySREntryDataset(SREntryDataset):
    """
    Reads examples from directory on the fly.
    Example of directory structure that must be preserved if deepness is not set.
    Setting allowed_deepness parameter allows to have more subdirectories between
    the root directory and actual examples. Directories marked with '===' are optional.
    """

    def __init__(self, lr_dir: str, hr_dir: str = None, hr_mask_dir: str = None, additional_deepness: int = 0, bands: Union[str, List[str]] = None,
                 allowed_dirs: list = None, hr_name: str = '', lr_images=None):
        super().__init__()
        self._lr_dir = lr_dir
        self._hr_dir = hr_dir
        self._hr_mask_dir = hr_mask_dir
        self._hrs = None
        self._hr_masks = None
        self.lr_images = lr_images
        self.bands = bands
        self.hr_name = hr_name
        self._name_indices = self.__get_entry_name_indices(additional_deepness)
        self.allowed_dirs = allowed_dirs if allowed_dirs is not None else []
        self._lrs = [Path(sb.path) for sb in DirectoryContents.deep_list_subdirs(self._lr_dir, deepness=additional_deepness)]
        lr_names = [lr.name for lr in self._lrs]
        if hr_dir is not None:
            self._hrs = [Path(sb.path) for sb in DirectoryContents.deep_list_subdirs(self._hr_dir, deepness=additional_deepness)]
            for hr_path in self._hrs:
                assert hr_path.name in lr_names, f"HR directory {hr_path} does not have corresponding LR directory."
        if hr_mask_dir is not None:
            self._hr_masks = [Path(sb.path) for sb in DirectoryContents.deep_list_subdirs(self._hr_mask_dir, deepness=additional_deepness)]
            for hr_mask_path in self._hr_masks:
                assert hr_mask_path.name in lr_names, f"HR mask directory {hr_mask_path} does not have corresponding LR directory."
        self._transformations = []


    def __get_entry_name_indices(self, additional_deepness):
        name_start_index = 0
        name_end_index = 1
        if self.bands is not None:
            name_start_index -= 1
            name_end_index -= 1
        name_start_index -= additional_deepness
        if name_end_index == 0:
            name_end_index = None
        return name_start_index, name_end_index

    def __len__(self):
        return len(self._lrs)

    def get_raw_examples(self):
        return self._lrs, self._hrs, self._hr_masks

    def __getitem__(self, item) -> Union[SREntry, MultiBandSREntry]:
        example_path = self._lrs[item]
        if isinstance(self.bands, str):
            return self._read_data(example_path, self.bands)
        elif isinstance(self.bands, list):
            return MultiBandSREntry({band: self._read_data(example_path, band) for band in self.bands})
        return self._read_data(example_path)

    def _read_data(self, example_path, band=None) -> SREntry:
        hr_image, lr_masks, hr_mask = None, None, None
        lr_images = list(example_path.glob(f"{band}.*"))
        assert len(lr_images) == 1, f"LR image for band {band} not found in {example_path} or too many images found."
        lr_images = [io.read_image(lr) for lr in lr_images]

        entry_name = '_'.join(list(example_path.parts)[self._name_indices[0]:self._name_indices[1]])
        sr_entry = SREntry(hr_image=hr_image, lr_images=lr_images, hr_mask=hr_mask, lr_masks=lr_masks,
                           name=entry_name, band=band)
        for transformation in self._transformations:
            sr_entry = transformation(sr_entry)
        return sr_entry

    def transform(self, *transformations: SREntryTransformation) -> "DistributedLazySREntryDataset":
        copied = copy.copy(self)
        copied._transformations = copy.copy(self._transformations)
        copied._transformations += transformations
        return copied