import os
from enum import Enum
from typing import Union, List, Tuple

from superdeep.constants import S2_BANDS
from superdeep.data import MultiBandSREntry, SREntry, EntryElements
from superdeep.data.datasets import StaticSREntryDataset, LazySREntryDataset, ConcatenatedSREntryDataset
import superdeep.data.transformations as dtrnf
import superdeep.image.transformations as itrnf


class SubsetEnum(Enum):
    TRAIN = 0
    VALID = 1
    TEST = 2
    ALL = 3


class DatasetConfigurator:
    """
    Base class for all dataset configurators. The configurators serve as input pipeline templates to load, preprocess and (potentially) divide dataset into subsets.
    """
    root = r'..\dataset'

    def __init__(self, root: str, input_bands=None, lr_images: int = None):
        if root is not None:
            self.root = root
        if input_bands is None:
            input_bands = self.bands()
        self.used_bands = input_bands
        self.lr_images = lr_images

    @classmethod
    def bands(cls):
        raise NotImplementedError

    @property
    def name(self):
        raise NotImplementedError

    def _prepare_dataset(self, subset, register_lrs):
        raise NotImplementedError

    def _get_subset(self, dataset, subset):
        return dataset

    def _preprocess(self, dataset):
        return dataset

    def get_dataset(self, subset: SubsetEnum, register_lrs: bool = False):
        dataset = self._prepare_dataset(subset, register_lrs)
        dataset = self._preprocess(dataset)
        dataset = self._get_subset(dataset, subset)
        return dataset


def _subset_string(subset) -> str:
    return ['train', 'val', 'test'][subset.value]


class ProbaV(DatasetConfigurator):
    """
    Configurator that loads, divides into subsets and preprocess ProbaV dataset.
    May also be applied to real and semi-simulated datasets.

    :arg input_bands: List of bands (as strings) to read from the disk.
    :arg root: Root directory where the dataset is stored.
    :arg name: Name of the dataset to load. It has to be located in root directory.
    """
    origin_dir = ''

    def __init__(self, root: str, input_bands: str, name: str = None, normalize: Tuple[bool, str] = False, **kwargs):
        super(ProbaV, self).__init__(root, input_bands)
        self.bands = input_bands
        self.dataset_name = name
        self.normalize = normalize

    @property
    def name(self):
        if self.dataset_name is None:
            return 'ProbaV'
        return self.dataset_name

    def _get_subset(self, dataset, subset):
        test_train_ratio = 0.2
        val_train_ratio = 0.2
        entire_dataset = dataset.shuffle(0)
        all_examples = len(entire_dataset)
        test_examples = int(all_examples * test_train_ratio)
        valid_examples = int((all_examples - test_examples) * val_train_ratio)
        train_examples = all_examples - test_examples - valid_examples
        test_dataset = entire_dataset.take(test_examples)
        valid_dataset = entire_dataset.take(valid_examples, test_examples)
        train_dataset = entire_dataset.take(train_examples, test_examples + valid_examples)
        dataset = {SubsetEnum.TRAIN: train_dataset,
                   SubsetEnum.TEST: test_dataset,
                   SubsetEnum.VALID: valid_dataset,
                   SubsetEnum.ALL: entire_dataset}.get(subset)
        return dataset

    def _prepare_dataset(self, subset, register_lrs):
        dataset = None
        for band in self.bands:
            temp_dataset = LazySREntryDataset(
                os.path.join(self.root, self.name, 'train', band),
                additional_deepness=0)
            if dataset is None:
                dataset = temp_dataset
            else:
                dataset = ConcatenatedSREntryDataset(dataset, temp_dataset)
        if register_lrs:
            dataset = StaticSREntryDataset([dtrnf.register_entry(entry) for entry in dataset])
        return dataset

    def _preprocess(self, dataset):
        to_float = dtrnf.SREntryTransformationBuilder() \
            .with_transformation(itrnf.ToFloat(), target=[EntryElements.LR, EntryElements.HR,
                                                          EntryElements.LR_MASKS, EntryElements.HR_MASK]) \
            .without_rest_transformation() \
            .build()

        standardize = dtrnf.SREntryTransformationBuilder() \
            .with_transformation(itrnf.Divide(2 ** 14 - 1), target=[EntryElements.LR, EntryElements.HR]) \
            .without_rest_transformation() \
            .build()

        convert_masks = dtrnf.SREntryTransformationBuilder() \
            .with_transformation(itrnf.Binarize(0.5), target=[EntryElements.LR_MASKS, EntryElements.HR_MASK]) \
            .without_rest_transformation() \
            .build()

        norm_target = [EntryElements.LR, EntryElements.HR]
        if self.normalize == 'lrs':
            norm_target = [EntryElements.LR]
        elif self.normalize == 'hr':
            norm_target = [EntryElements.HR]

        normalize = dtrnf.SREntryTransformationBuilder() \
            .with_transformation(itrnf.Normalize(), target=norm_target) \
            .without_rest_transformation() \
            .build()
        dataset = dataset.transform(to_float)
        dataset = dataset.transform(standardize)
        if self.normalize:
            dataset = dataset.transform(normalize)
        dataset = dataset.transform(convert_masks)
        return dataset


class S2_Configurator(DatasetConfigurator):
    def __init__(self, root, bands: list, name: str, lr_images: int = None):
        super().__init__(root, bands, lr_images=lr_images)
        self._name = name
        self.path = os.path.join(self.root, self.name)
        assert os.path.isdir(self.path), f"No such directory: {self.path}"

    @property
    def name(self):
        return self._name

    @classmethod
    def bands(cls):
        return S2_BANDS

    def _prepare_dataset(self, subset, register_lrs):
        raise NotImplementedError


class S2_Artificial(S2_Configurator):
    """
    Configurator that loads, divides into subsets and preprocess Sentinel-2 artificial datasets.
    May also be applied to real and semi-simulated datasets.

    :arg input_bands: List of bands (as strings) to read from the disk.
    :arg root: Root directory where the dataset is stored.
    :arg name: Name of the dataset to load. It has to be located in root directory.
    :arg additional_deepness: Set to 0 if examples are stored directly under dataset_name folder. If they are placed deeper please increase this value accordingly.
    :arg hr_name: Empty string or accurate HR name if there is only one HR per band. Set to None if scenes have no HR image. Otherwise enter a name of HR you want to use to calculate metrics assuming all scenes have the HR images with the same names.
    :arg lr_images: Number of LR images to load perr example.
    :arg divide: All pixel values will be divided by this value. If None the raw data is loaded.
    :arg normalize: True if you want the data to be normalized using Z-score.
    """
    def __init__(self, input_bands: list = None, root: str = None, name: str = None,
                 divide: Union[float, int, None] = None, normalize: bool = False, additional_deepness: int = 1,
                 hr_name: Union[str, None] = '', lr_images: int = None, mask_lrs: bool = False,
                 register_lrs: bool = False, **args):
        super(S2_Artificial, self).__init__(root, input_bands, name, lr_images)
        self.divide = divide
        self.normalize = normalize
        self.deepness = additional_deepness
        self.hr_name = hr_name
        self.mask_lrs = mask_lrs
        self.register_lrs = register_lrs

    def _prepare_dataset(self, subset, register_lrs):
        dataset = LazySREntryDataset(self.path, additional_deepness=self.deepness,
                                     bands=self.used_bands, hr_name=self.hr_name, lr_images=self.lr_images)
        return dataset

    def _get_subset(self, dataset, subset):
        test_train_ratio = 0.2
        val_train_ratio = 0.2
        entire_dataset = dataset.shuffle(0)
        all_examples = len(entire_dataset)
        test_examples = int(all_examples * test_train_ratio)
        valid_examples = int((all_examples - test_examples) * val_train_ratio)
        train_examples = all_examples - test_examples - valid_examples
        test_dataset = entire_dataset.take(test_examples)
        valid_dataset = entire_dataset.take(valid_examples, test_examples)
        train_dataset = entire_dataset.take(train_examples, test_examples + valid_examples)
        dataset = {SubsetEnum.TRAIN: train_dataset,
                   SubsetEnum.TEST: test_dataset,
                   SubsetEnum.VALID: valid_dataset,
                   SubsetEnum.ALL: entire_dataset}.get(subset)
        return dataset

    def _preprocess(self, dataset):
        to_float = dtrnf.SREntryTransformationBuilder() \
            .with_transformation(itrnf.ToFloat(), target=[EntryElements.LR, EntryElements.HR]) \
            .without_rest_transformation() \
            .build()

        binarize_masks = dtrnf.SREntryTransformationBuilder()\
            .with_transformation(itrnf.Binarize(), target=[EntryElements.LR_MASKS, EntryElements.HR_MASK])\
            .without_rest_transformation()\
            .build()

        norm_target = [EntryElements.LR, EntryElements.HR]
        if self.normalize == 'lrs':
            norm_target = [EntryElements.LR]
        elif self.normalize == 'hr':
            norm_target = [EntryElements.HR]

        normalize = dtrnf.SREntryTransformationBuilder() \
            .with_transformation(itrnf.Normalize(), target=norm_target) \
            .without_rest_transformation() \
            .build()

        dataset = dataset.transform(to_float)
        dataset = dataset.transform(binarize_masks)
        if self.register_lrs:
            dataset = dataset.transform(dtrnf.register_entry)
        if self.mask_lrs:
            dataset = dataset.transform(dtrnf.mask_lrs)
        if self.divide is not None:
            divide = dtrnf.SREntryTransformationBuilder() \
                .with_transformation(itrnf.Divide(self.divide), target=[EntryElements.LR, EntryElements.HR]) \
                .without_rest_transformation() \
                .build()
            dataset = dataset.transform(divide)
        if self.normalize:
            dataset = dataset.transform(normalize)
        return dataset


class S2FromModelPredictions(S2_Configurator):
    """
    Configurator that loads, divides into subsets and preprocess Sentinel-2 artificial datasets.
    May also be applied to real and semi-simulated datasets.

    :arg input_bands: List of bands (as strings) to read from the disk.
    :arg root: Root directory where the dataset is stored.
    :arg name: Name of the dataset to load. It has to be located in root directory.
    :arg additional_deepness: Set to 0 if examples are stored directly under dataset_name folder. If they are placed deeper please increase this value accordingly.
    :arg hr_name: Empty string or accurate HR name if there is only one HR per band. Set to None if scenes have no HR image. Otherwise enter a name of HR you want to use to calculate metrics assuming all scenes have the HR images with the same names.
    :arg lr_images: Number of LR images to load perr example.
    :arg divide: All pixel values will be divided by this value. If None the raw data is loaded.
    :arg normalize: True if you want the data to be normalized using Z-score.
    """
    def __init__(self, lr_dir: str, hr_dir=None, hr_mask_dir=None, input_bands: list = None, root: str = None, name: str = None,
                 divide: Union[float, int, None] = None, normalize: bool = False, additional_deepness: int = 0,
                 lr_images: int = 1, mask_lrs: bool = False, register_lrs: bool = False, **args):
        super(S2FromModelPredictions, self).__init__(root, input_bands, name, lr_images)
        self.divide = divide
        self.normalize = normalize
        self.deepness = additional_deepness
        self.mask_lrs = mask_lrs
        self.register_lrs = register_lrs
        self.lr_dir = lr_dir
        self.hr_dir = hr_dir
        self.hr_mask_dir = hr_mask_dir

    def _prepare_dataset(self, subset, register_lrs):
        from superdeep.data.datasets import DistributedLazySREntryDataset
        dataset = DistributedLazySREntryDataset(self.lr_dir, self.hr_dir, self.hr_mask_dir,
                                                additional_deepness=self.deepness,
                                                bands=self.used_bands, lr_images=self.lr_images)
        return dataset

    def _get_subset(self, dataset, subset):
        test_train_ratio = 0.2
        val_train_ratio = 0.2
        entire_dataset = dataset.shuffle(0)
        all_examples = len(entire_dataset)
        test_examples = int(all_examples * test_train_ratio)
        valid_examples = int((all_examples - test_examples) * val_train_ratio)
        train_examples = all_examples - test_examples - valid_examples
        test_dataset = entire_dataset.take(test_examples)
        valid_dataset = entire_dataset.take(valid_examples, test_examples)
        train_dataset = entire_dataset.take(train_examples, test_examples + valid_examples)
        dataset = {SubsetEnum.TRAIN: train_dataset,
                   SubsetEnum.TEST: test_dataset,
                   SubsetEnum.VALID: valid_dataset,
                   SubsetEnum.ALL: entire_dataset}.get(subset)
        return dataset

    def _preprocess(self, dataset):
        to_float = dtrnf.SREntryTransformationBuilder() \
            .with_transformation(itrnf.ToFloat(), target=[EntryElements.LR, EntryElements.HR]) \
            .without_rest_transformation() \
            .build()

        binarize_masks = dtrnf.SREntryTransformationBuilder()\
            .with_transformation(itrnf.Binarize(), target=[EntryElements.LR_MASKS, EntryElements.HR_MASK])\
            .without_rest_transformation()\
            .build()

        norm_target = [EntryElements.LR, EntryElements.HR]
        if self.normalize == 'lrs':
            norm_target = [EntryElements.LR]
        elif self.normalize == 'hr':
            norm_target = [EntryElements.HR]

        normalize = dtrnf.SREntryTransformationBuilder() \
            .with_transformation(itrnf.Normalize(), target=norm_target) \
            .without_rest_transformation() \
            .build()

        dataset = dataset.transform(to_float)
        dataset = dataset.transform(binarize_masks)
        if self.register_lrs:
            dataset = dataset.transform(dtrnf.register_entry)
        if self.mask_lrs:
            dataset = dataset.transform(dtrnf.mask_lrs)
        if self.divide is not None:
            divide = dtrnf.SREntryTransformationBuilder() \
                .with_transformation(itrnf.Divide(self.divide), target=[EntryElements.LR, EntryElements.HR]) \
                .without_rest_transformation() \
                .build()
            dataset = dataset.transform(divide)
        if self.normalize:
            dataset = dataset.transform(normalize)
        return dataset

class MuS2(S2_Configurator):
    """
    Configurator that loads, divides into subsets and preprocess Sentinel-2 artificial datasets.
    May also be applied to real and semi-simulated datasets.

    :arg input_bands: List of bands (as strings) to read from the disk.
    :arg root: Root directory where the dataset is stored.
    :arg name: Name of the dataset to load. It has to be located in root directory.
    :arg additional_deepness: Set to 0 if examples are stored directly under dataset_name folder. If they are placed deeper please increase this value accordingly.
    :arg hr_name: Empty string or accurate HR name if there is only one HR per band. Set to None if scenes have no HR image. Otherwise enter a name of HR you want to use to calculate metrics assuming all scenes have the HR images with the same names.
    :arg lr_images: Number of LR images to load perr example.
    :arg divide: All pixel values will be divided by this value. If None the raw data is loaded.
    :arg normalize: True if you want the data to be normalized using Z-score.
    """
    def __init__(self, input_bands: list = None, root: str = None, name: str = None,
                 divide=None, normalize: bool = False, additional_deepness: int = 0,
                 hr_name: Union[str, None] = '', lr_images: int = None, mask_lrs: bool = False,
                 register_lrs: bool = False, **args):
        super(MuS2, self).__init__(root, input_bands, name, lr_images)
        self.divide = divide
        self.normalize = normalize
        self.deepness = additional_deepness
        self.hr_name = hr_name
        self.mask_lrs = mask_lrs
        self.register_lrs = register_lrs

    def _prepare_dataset(self, subset, register_lrs):
        dataset = LazySREntryDataset(self.path, additional_deepness=self.deepness,
                                     bands=self.used_bands, hr_name=self.hr_name, lr_images=self.lr_images)
        return dataset

    def _get_subset(self, dataset, subset):
        test_train_ratio = 0.2
        val_train_ratio = 0.2
        entire_dataset = dataset.shuffle(0)
        all_examples = len(entire_dataset)
        test_examples = int(all_examples * test_train_ratio)
        valid_examples = int((all_examples - test_examples) * val_train_ratio)
        train_examples = all_examples - test_examples - valid_examples
        test_dataset = entire_dataset.take(test_examples)
        valid_dataset = entire_dataset.take(valid_examples, test_examples)
        train_dataset = entire_dataset.take(train_examples, test_examples + valid_examples)
        dataset = {SubsetEnum.TRAIN: train_dataset,
                   SubsetEnum.TEST: test_dataset,
                   SubsetEnum.VALID: valid_dataset,
                   SubsetEnum.ALL: entire_dataset}.get(subset)
        return dataset

    def _preprocess(self, dataset):
        from omegaconf.listconfig import ListConfig
        to_float = dtrnf.SREntryTransformationBuilder() \
            .with_transformation(itrnf.ToFloat(), target=[EntryElements.LR, EntryElements.HR]) \
            .without_rest_transformation() \
            .build()

        binarize_masks = dtrnf.SREntryTransformationBuilder()\
            .with_transformation(itrnf.Binarize(), target=[EntryElements.LR_MASKS, EntryElements.HR_MASK])\
            .without_rest_transformation()\
            .build()

        norm_target = [EntryElements.LR, EntryElements.HR]
        if self.normalize == 'lrs':
            norm_target = [EntryElements.LR]
        elif self.normalize == 'hr':
            norm_target = [EntryElements.HR]

        normalize = dtrnf.SREntryTransformationBuilder() \
            .with_transformation(itrnf.Normalize(), target=norm_target) \
            .without_rest_transformation() \
            .build()

        dataset = dataset.transform(to_float)
        dataset = dataset.transform(binarize_masks)
        if self.register_lrs:
            dataset = dataset.transform(dtrnf.register_entry)
        if self.mask_lrs:
            dataset = dataset.transform(dtrnf.mask_lrs)
        if self.divide is not None:
            if isinstance(self.divide, list) or isinstance(self.divide, ListConfig):
                assert len(self.divide) == 2, 'Divide list must have 2 elements.'
                lr_divide = self.divide[0]
                hr_divide = self.divide[1]
            else:
                lr_divide = self.divide
                hr_divide = self.divide
            divide = dtrnf.SREntryTransformationBuilder() \
                .with_transformation(itrnf.Divide(lr_divide), target=[EntryElements.LR]) \
                .without_rest_transformation() \
                .build()
            dataset = dataset.transform(divide)

            divide = dtrnf.SREntryTransformationBuilder() \
                .with_transformation(itrnf.Divide(hr_divide), target=[EntryElements.HR]) \
                .without_rest_transformation() \
                .build()
            dataset = dataset.transform(divide)
        if self.normalize:
            dataset = dataset.transform(normalize)
        return dataset