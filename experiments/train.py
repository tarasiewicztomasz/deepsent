import os
import importlib
import hydra
from hydra.core.hydra_config import HydraConfig
import wandb
import omegaconf
import logging
from pathlib import Path
import shutil
import torch
from torch.utils import data
import torchdatasets as td
from superdeep.training import trainers, runners
from superdeep.data.transformations import PatchConfig, PatchSREntryDataset
from superdeep.data.configurator import SubsetEnum
from superdeep.utilities import observers, configs


@hydra.main(config_name="train-config", config_path="configs", version_base=None)
def main(cfg):
    log = logging.getLogger()
    log.removeHandler(log.handlers[0])
    os.environ['WANDB_DISABLED'] = cfg.wandb.disable_wandb
    if cfg.wandb.group_name == 'None':
        cfg.wandb.group_name = None

    run_dir = Path(HydraConfig.get().run.dir).resolve()
    wandb.init(project=cfg.wandb.project, entity=cfg.wandb.entity, name=cfg.wandb.run_name,
               id=cfg.wandb.run_id, group=cfg.wandb.group_name, notes=cfg.wandb.notes,
               dir=str(run_dir), tags=cfg.wandb.tags, job_type='training', anonymous='allow',
               config=omegaconf.OmegaConf.to_container(cfg, resolve=True, throw_on_missing=True))
    print(f"Run ID: \t{wandb.run.id}")
    print(f"Run name: \t{wandb.run.name}")
    print(f"Run path: \t{wandb.run.path}")
    print(f"Group name: \t{wandb.run.group}")

    device = f'cuda:{cfg.system.device}' if torch.cuda.is_available() else 'cpu'
    print(f"Device: {device}")
    model = getattr(importlib.import_module(cfg.model.module),
                    cfg.model.name)(**({} if cfg.model.params is None else cfg.model.params))
    model.to(device)
    input_bands = configs.str_to_bands(cfg.model.input_bands)
    output_bands = configs.str_to_bands(cfg.model.output_bands)
    print(f"Input bands: {input_bands}")
    print(f"Output bands: {output_bands}")
    cfg.dataset.params.root = Path(cfg.dataset.params.root).resolve()
    configurator = getattr(importlib.import_module(cfg.dataset.configurator.module),
                           cfg.dataset.configurator.name)(input_bands=input_bands, **cfg.dataset.params)
    optimizer = getattr(importlib.import_module(cfg.optimizer.module),
                        cfg.optimizer.name)(model.parameters(), lr=cfg.params.lr)
    loss_fn = getattr(importlib.import_module("superdeep.training.loss"),
                      cfg.loss.name)(**cfg.loss.params)
    collate_fn = getattr(importlib.import_module(cfg.dataset.collation.module),
                         cfg.dataset.collation.name)

    if cfg.model.weights_file not in [None, 'None', '']:
        # print absolute path
        x = Path(cfg.model.weights_file).resolve()
        print(f"Loading model from: {x.absolute()}")
        loaded_files = torch.load(cfg.model.weights_file)
        model.load_state_dict(loaded_files['model'])
        # optimizer.load_state_dict(loaded_files['optimizer'])

    patch_size = cfg.params.patch_size
    hr_config = PatchConfig((patch_size, patch_size), (cfg.params.patch_stride, cfg.params.patch_stride))

    print("Loading dataset")
    train_dataset = configurator.get_dataset(SubsetEnum.TRAIN, register_lrs=cfg.dataset.params.register_lrs)
    print("Train examples: ", len(train_dataset))
    if cfg.params.patch_size is not None:
        train_dataset = PatchSREntryDataset(hr_config)(train_dataset)
        print("Train patches: ", len(train_dataset))

    val_dataset = configurator.get_dataset(SubsetEnum.VALID, register_lrs=cfg.dataset.params.register_lrs)

    cache_path = Path(cfg.system.cache_dir).resolve() / 'cache' / device.replace(':', '_')
    print(f"Cache path: ", cache_path.absolute())
    os.makedirs(cache_path, exist_ok=True)
    if os.path.isdir(cache_path) and cfg.system.recache:
        shutil.rmtree(cache_path)

    # Set up dataset caching
    train_dataset = train_dataset.cache(td.cachers.Pickle(cache_path / 'train'))
    val_dataset = val_dataset.cache(td.cachers.Pickle(cache_path / 'val'))

    # Initialize data loaders
    train_loader = data.DataLoader(train_dataset, batch_size=cfg.params.batch_size, shuffle=True,
                                   collate_fn=collate_fn(output_bands=output_bands,
                                                         random_band=cfg.params.single_output_training,
                                                         **cfg.dataset.collation.params))
    validation_loader = data.DataLoader(val_dataset, batch_size=1, shuffle=False,
                                        collate_fn=collate_fn(output_bands=output_bands,
                                                              random_band=False, **cfg.dataset.collation.params))

    wandb.watch(model, criterion=loss_fn, log="all", log_freq=len(train_dataset) / cfg.params.batch_size,
                log_graph=True)

    train_runner = runners.TrainingEpochRunner(optimizer, criterion=loss_fn, device=device)
    train_runner.add_observer(observers.ProgressLogger("T"))
    val_runner = runners.ValidationEpochRunner(criterion=loss_fn, device=device)
    val_runner.add_observer(observers.ProgressLogger("V"))
    val_runner.add_observer(observers.FirstValidationBatchSaver())

    trainer = trainers.PyTorchTrainer(model, train_runner, val_runner)
    trainer.add_observer(observers.LossLogger())
    trainer.add_observer(observers.PyTorchModelSaver())
    trainer.perform_training(cfg.params.epochs, train_loader, validation_loader)

    wandb.finish()


if __name__ == "__main__":
    main()
