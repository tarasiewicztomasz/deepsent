import os
import importlib
from pathlib import Path
import shutil
import hydra
from hydra.utils import get_original_cwd
from hydra.core.hydra_config import HydraConfig
import wandb
import omegaconf
import time
from tqdm import tqdm
from skimage.exposure import match_histograms

from superdeep.data.transformations import PatchConfig, PatchSREntryDataset
from superdeep.utilities import configs, converters
from superdeep.utilities.benchmark.saving import ImageLogger, ImageSaver, TableLogger, TableRow
from superdeep.data.entry import MultiBandSREntry
import torch
from torch.utils import data
import torch.nn.functional as F
import torchdatasets as td
import logging

@hydra.main(config_name="test-config", config_path="configs", version_base=None)
def main(cfg):
    log = logging.getLogger()
    log.removeHandler(log.handlers[0])
    os.environ['WANDB_DISABLED'] = cfg.wandb.disable_wandb
    run_dir = Path(HydraConfig.get().run.dir).resolve()
    wandb.init(id=cfg.wandb.benchmark_id, job_type="test", entity=cfg.wandb.entity, project=cfg.wandb.project,
               dir=run_dir, tags=cfg.wandb.tags, name=cfg.wandb.run_name,
               config=omegaconf.OmegaConf.to_container(cfg, resolve=True, throw_on_missing=True), anonymous='allow')

    print(f"Run ID: \t{wandb.run.id}")
    print(f"Run name: \t{wandb.run.name}")
    print(f"Run path: \t{wandb.run.path}")
    print(f"Group name: \t{wandb.run.group}")


    print(f"Testing: {wandb.run.name}")
    device = f'cuda:{cfg.system.device}' if (torch.cuda.is_available() and cfg.system.device is not None) else 'cpu'
    print(f"Device: {device}")

    model = getattr(importlib.import_module(cfg.model.module),
                    cfg.model.name)(**(dict() if cfg.model.params is None else cfg.model.params))

    loaded_files = torch.load(Path(cfg.model.weights_file).resolve())
    model.load_state_dict(loaded_files['model'])
    model.to(device)
    model.eval()
    input_bands = configs.str_to_bands(cfg.model.input_bands if cfg.dataset.input_bands is None
                                       else cfg.dataset.input_bands)
    output_bands = configs.str_to_bands(cfg.model.output_bands if cfg.dataset.output_bands is None
                                        else cfg.dataset.output_bands)
    configurator = getattr(importlib.import_module(cfg.dataset.configurator.module),
                           cfg.dataset.configurator.name)(input_bands=input_bands, **cfg.dataset.params)
    collate_fn = getattr(importlib.import_module(cfg.dataset.collation.module),
                         cfg.dataset.collation.name)

    patch_size = cfg.params.patch_size
    hr_config = PatchConfig((patch_size, patch_size), (cfg.params.patch_stride, cfg.params.patch_stride))

    print("Loading dataset")
    subset = converters.str_to_subset(cfg.dataset.subset)
    test_dataset = configurator.get_dataset(subset, register_lrs=cfg.dataset.params.register_lrs)

    if cfg.params.patch_size is not None:
        test_dataset = PatchSREntryDataset(hr_config)(test_dataset)
    print("test examples: ", len(test_dataset))

    cache_path = Path(cfg.system.cache_dir).resolve() / 'cache' / device.replace(':', '_')
    print(f"Caching to: {cache_path.absolute()}")
    os.makedirs(cache_path, exist_ok=True)
    if os.path.isdir(cache_path) and cfg.system.recache:
        shutil.rmtree(cache_path)

    test_dataset = test_dataset.cache(td.cachers.Pickle(cache_path / 'test'))
    test_loader = data.DataLoader(test_dataset, batch_size=1, shuffle=False,
                                  collate_fn=collate_fn(output_bands=output_bands,
                                                        random_band=False, permute=False))

    metrics = {}
    if cfg.metrics is not None:
        metrics = {metric.name: getattr(importlib.import_module(metric.module),
                                        metric.name)(**(metric.params if isinstance(metric.params, dict) else {}))
                   for metric in cfg.metrics}

    results = {metric: {'total': []} for metric in metrics.keys()}

    out_dir = Path(get_original_cwd()) / cfg.save_params.out_dir
    image_logger = ImageLogger(cfg.wandb.images_to_save)
    table_logger = TableLogger().set_id('example_name')
    image_saver = ImageSaver(out_dir, wandb.run.name, cfg.save_params.save_predictions_locally)
    hr_saver = ImageSaver(out_dir, "HR", cfg.save_params.save_hr)
    hr_mask_saver = ImageSaver(out_dir, "HR_MASK", cfg.save_params.save_hr)
    lr_saver = ImageSaver(out_dir, "LR", cfg.save_params.save_lr)

    # BENCHMARK LOOP
    with torch.no_grad():
        for iterator, entry in enumerate(tqdm(test_loader)):
            entry.to(device)
            start_time = time.time()
            output = model(entry)
            if not isinstance(entry, MultiBandSREntry) and entry.band is not None:
                output = {entry.band: output}
                entry.hr_image = {entry.band: entry.hr_image}
                entry.hr_mask = {entry.band: entry.hr_mask}
                entry.lr_images = {entry.band: entry.lr_images}
                entry.lr_masks = {entry.band: entry.lr_masks}
            if cfg.postprocessing.upscale_to_hr:
                for band in entry.hr_image.keys():
                    if entry.hr_image[band] is not None:
                        output[band] = F.interpolate(output[band], size=entry.hr_image[band].shape[-2:],
                                                     mode='bicubic', align_corners=False)
            end_time = time.time() - start_time
            if cfg.postprocessing.match_histogram:
                output = match_hist_to_hr(output, entry.hr_image)
            if cfg.postprocessing.destandardize:
                for k, v in output.items():
                    v = (v - v.mean()) / v.std()
                    output[k] = v * entry.hr_image[k].std() + entry.hr_image[k].mean()
            image_logger.add(output, entry.name[0])
            image_saver.save(output, entry.name[0])
            hr_saver.save(entry.hr_image, entry.name[0])
            hr_mask_saver.save(entry.hr_mask, entry.name[0])
            lr_saver.save({band: entry.lr_images[band] for band in entry.lr_images.keys()},
                          entry.name[0])
            row = TableRow().add_data(entry.name[0], 'example_name')
            for band in output.keys():
                row.add_data(end_time, f'time_{band}')
            for metric_name, metric_fn in metrics.items():
                example_score = metric_fn(output, entry.hr_image, hr_mask=entry.hr_mask)
                if isinstance(example_score, dict) and len(example_score) > 0:
                    for band, value in example_score.items():
                        if band not in results[metric_name]:
                            results[metric_name][band] = []
                        results[metric_name][band].append(value.cpu().numpy().item())
                        row.add_data(value.cpu().numpy().item(), metric_name, band, )
                    example_score = [x.cpu().numpy().item() for x in example_score.values()]
                if len(example_score) > 0:
                    results[metric_name]['total'].extend(example_score)
            table_logger.add_row(row)
    table_logger.log()
    image_logger.log()
    wandb.finish()


def match_hist_to_hr(sr, hr):
    def match_hist(sr, hr):
        sr = sr.squeeze().cpu().numpy()
        sr = match_histograms(sr, hr.squeeze().cpu().numpy())
        return torch.from_numpy(sr).unsqueeze(0).unsqueeze(0).to(hr.device)

    if isinstance(sr, dict):
        return {band: match_hist(sr[band], hr[band]) if hr[band] is not None else sr[band] for band in sr.keys()}
    else:
        return match_hist(sr, hr)


if __name__ == "__main__":
    main()